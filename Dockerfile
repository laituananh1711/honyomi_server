FROM node:fermium-alpine3.14 AS builder

WORKDIR /app

COPY package.json yarn.lock /app/

# TODO: use --only-dev when it is available
RUN yarn install --frozen-lockfile

COPY . .

RUN yarn run build

FROM node:fermium-alpine3.14
RUN apk add dumb-init

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /app

COPY package.json yarn.lock /app/

RUN yarn install --prod

COPY --from=builder --chown=node:node /app/dist ./dist

USER node

EXPOSE 3000

CMD ["dumb-init", "yarn", "start:prod"]

