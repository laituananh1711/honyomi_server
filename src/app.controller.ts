import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { LanguageCodeToName, languageCodeToName } from './assets/lang';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('languages')
  getLanguage(): { data: LanguageCodeToName } {
    return { data: languageCodeToName };
  }

  @Get('health/live')
  getHealthLive() {
    return { content: 'OK' };
  }
}
