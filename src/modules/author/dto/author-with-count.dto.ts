import { Author } from '../schemas/author.schema';

export class AuthorWithCountDto {
  author: Author;
  mangaCount: number;
}
