import { Type } from 'class-transformer';
import { IsOptional, IsString, ValidateNested } from 'class-validator';
import { OffsetPagingQuery } from '../../../utils/paging-query';

export class FindAuthorFilterQuery {
  @IsString()
  @IsOptional()
  name?: string;
}

export class FindAuthorQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  sort: string[] = [];

  @ValidateNested()
  @Type(() => FindAuthorFilterQuery)
  @IsOptional()
  filter?: FindAuthorFilterQuery;
}
