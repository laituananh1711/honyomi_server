import { Optional } from '@nestjs/common';
import { IsNotEmpty } from 'class-validator';

export class CreateAuthorDto {
  @IsNotEmpty()
  name: string;

  @Optional()
  description: string;
}
