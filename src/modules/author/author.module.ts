import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Author, AuthorSchema } from './schemas/author.schema';
import { AuthorController } from './author.controller';
import { AuthorService } from './author.service';
import { MangaModule } from '../manga/manga.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Author.name, schema: AuthorSchema }]),
    MangaModule,
  ],
  exports: [MongooseModule],
  controllers: [AuthorController],
  providers: [AuthorService],
})
export class AuthorModule {}
