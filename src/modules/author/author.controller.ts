import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { RoleType } from '../../constant';
import { PaginatedResultsMeta } from '../../utils/paginate';
import { parseSortQuery } from '../../utils/parse-sort-query';
import { PromiseJSONResponse } from '../../utils/types';
import { Roles } from '../auth/decorators/role.decorator';
import { AuthorService } from './author.service';
import { FindAuthorQuery } from './dto/author-query';
import { AuthorWithCountDto } from './dto/author-with-count.dto';
import { CreateAuthorDto } from './dto/create-author.dto';
import { UpdateAuthorDto } from './dto/update-author.dto';
import { Author } from './schemas/author.schema';

@Controller('authors')
export class AuthorController {
  constructor(private authorService: AuthorService) {}

  @Get()
  findAll(
    @Query() { perPage, page, sort, filter }: FindAuthorQuery,
  ): PromiseJSONResponse<Author[], PaginatedResultsMeta> {
    const allowedCols = ['name'];
    return this.authorService
      .findWithPaginate({
        page,
        perPage,
        sort: parseSortQuery(sort ?? [], { allowedCols }),
        filter: {
          name: filter?.name,
        },
      })
      .then((res) => ({ data: res.items, meta: res.meta }));
  }

  @Get('withMangaCount')
  findAllWithMangaCount(): PromiseJSONResponse<AuthorWithCountDto[]> {
    return this.authorService
      .findAllWithMangaCount()
      .then((res) => ({ data: res }));
  }

  @Get(':id')
  getAuthor(@Param('id') id: string): PromiseJSONResponse<Author> {
    return this.authorService
      .find(id)
      .then((author) => {
        return {
          data: author,
        };
      })
      .catch((e) => {
        throw new BadRequestException(e);
      });
  }

  @Post()
  @Roles(RoleType.ADMIN)
  createAuthor(@Body() dto: CreateAuthorDto): PromiseJSONResponse<Author> {
    return this.authorService
      .create(dto)
      .then((author) => {
        return {
          data: author,
        };
      })
      .catch((e) => {
        throw new BadRequestException(e);
      });
  }

  @Put(':id')
  @Roles(RoleType.ADMIN)
  updateAuthor(
    @Param('id') id: string,
    @Body() dto: UpdateAuthorDto,
  ): PromiseJSONResponse<Author> {
    return this.authorService
      .update(id, dto)
      .then((author) => {
        return {
          data: author,
        };
      })
      .catch((e) => {
        throw new BadRequestException(e);
      });
  }

  @Delete(':id')
  @Roles(RoleType.ADMIN)
  deleteAuthor(@Param('id') id: string): PromiseJSONResponse<Author | null> {
    return this.authorService
      .deleteAuthor(id)
      .then((author) => {
        return {
          data: author,
        };
      })
      .catch((e) => {
        throw new BadRequestException(e);
      });
  }
}
