import { IsOptional } from 'class-validator';
import { SortOptions } from '../../utils/parse-sort-query';

export interface FindAuthorFilterOptions {
  name?: string;
}

export interface FindAuthorOptions {
  perPage: number;
  page: number;
  sort?: SortOptions;
  filter?: FindAuthorFilterOptions;
}
