import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Manga } from '../manga/schemas/manga.schema';
import { AuthorService } from './author.service';
import { Author } from './schemas/author.schema';

describe('AuthorService', () => {
  let service: AuthorService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthorService,
        {
          provide: getModelToken(Manga.name),
          useValue: {},
        },
        {
          provide: getModelToken(Author.name),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<AuthorService>(AuthorService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
