import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Manga, MangaDocument } from '../manga/schemas/manga.schema';
import { AuthorWithCountDto } from './dto/author-with-count.dto';
import { Author, AuthorDocument } from './schemas/author.schema';
import { Exception } from 'handlebars';
import { CreateAuthorDto } from './dto/create-author.dto';
import { UpdateAuthorDto } from './dto/update-author.dto';
import { paginate, PaginatedResults } from '../../utils/paginate';
import { FindAuthorOptions } from './types';

@Injectable()
export class AuthorService {
  constructor(
    @InjectModel(Author.name) private authorModel: Model<AuthorDocument>,
    @InjectModel(Manga.name) private mangaModel: Model<MangaDocument>,
  ) {}

  // findAll returns all authors.
  findAll(): Promise<Author[]> {
    return this.authorModel.find().exec();
  }

  findWithPaginate({
    perPage,
    page,
    filter,
    sort,
  }: FindAuthorOptions): Promise<PaginatedResults<Author>> {
    let query = this.authorModel.find(
      {},
      {
        name: 1,
        description: 1,
      },
    );

    if (filter != null) {
      if (filter.name != null && filter.name.length > 0) {
        query = query.find({
          name: new RegExp(filter.name, 'i'),
        });
      }
    }

    if (sort != null) {
      query = query.sort(sort);
    }

    return paginate(query, {
      page,
      perPage,
    });
  }

  // findAllWithMangaCount returns author detail and the number
  // of manga by this author.
  findAllWithMangaCount(): Promise<AuthorWithCountDto[]> {
    return this.mangaModel
      .aggregate([
        { $unwind: '$authors' },
        {
          $group: {
            _id: '$authors',
            count: { $sum: 1 },
          }, //group and count manga
        },
        {
          $lookup: {
            from: this.authorModel.collection.name,
            localField: '_id',
            foreignField: '_id',
            as: 'author',
          }, //left join authors field
        },
        { $unwind: '$author' },
        //Normalize response
        { $addFields: { 'author.id': '$author._id' } },
        { $project: { author: 1, mangaCount: '$count' } },
        {
          $project: {
            _id: 0,
            author: { _id: 0, __v: 0 },
          },
        },
      ])
      .exec();
  }

  find(id: string): Promise<Author> {
    return this.authorModel
      .findById(id)
      .exec()
      .then((author) => {
        if (!author) {
          throw new Exception('errors.authorNotExisted');
        }
        return author;
      });
  }

  create(dto: CreateAuthorDto): Promise<Author> {
    return this.authorModel.create(dto).then((author) => {
      return author;
    });
  }

  update(id: string, dto: UpdateAuthorDto): Promise<Author> {
    return this.authorModel
      .findByIdAndUpdate(id, dto, { new: true, upsert: true })
      .then((author) => {
        if (!author) {
          throw new Exception('errors.authorNotExisted');
        }
        return author;
      });
  }

  async deleteAuthor(id: string): Promise<Author | null> {
    const deletedAuthor = await this.authorModel.findByIdAndDelete(id).exec();

    await this.mangaModel.updateMany(
      {},
      {
        $pull: {
          authors: new Types.ObjectId(id),
        },
      },
    );

    return deletedAuthor;
  }
}
