import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Schema as MongooseSchema } from 'mongoose';
import { User } from '../../user/schemas/user.schema';
import { Notification } from './notification.schema';
import { NotificationStatusType } from '../../../constant';

@Schema({ timestamps: true })
export class NotificationStatus {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: User.name })
  user: User;

  @Prop({ type: MongooseSchema.Types.ObjectId, ref: Notification.name })
  notification: Notification;

  @Prop({
    default: NotificationStatusType.SENT,
    enum: Object.values(NotificationStatusType),
  })
  status: NotificationStatusType;
}

export const NotificationStatusSchema =
  SchemaFactory.createForClass(NotificationStatus);
