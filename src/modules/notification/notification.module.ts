import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  Notification,
  NotificationSchema,
} from './schemas/notification.schema';
import {
  NotificationStatus,
  NotificationStatusSchema,
} from './schemas/notification-status.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Notification.name, schema: NotificationSchema },
      { name: NotificationStatus.name, schema: NotificationStatusSchema },
    ]),
  ],
  exports: [MongooseModule],
})
export class NotificationModule {}
