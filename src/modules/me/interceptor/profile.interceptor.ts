import { CallHandler, ExecutionContext, NestInterceptor } from '@nestjs/common';
import { User } from '../../user/schemas/user.schema';
import { JSONResponse } from '../../../utils/types';
import { ProfileDto } from '../dto/profile.dto';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export class ProfileInterceptor
  implements NestInterceptor<User, JSONResponse<ProfileDto>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler<User>,
  ): Observable<JSONResponse<ProfileDto>> {
    return next.handle().pipe(map((user) => ({ data: new ProfileDto(user) })));
  }
}
