import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import * as mongoose from 'mongoose';
import { Token, TokenSchema } from '../auth/schemas/token.schema';
import { Favorite, FavoriteSchema } from '../user/schemas/favorite.schema';
import { User, UserSchema } from '../user/schemas/user.schema';
import { UserService } from '../user/user.service';
import { MeController } from './me.controller';

describe('MeController', () => {
  let controller: MeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [MeController],
      providers: [
        {
          provide: UserService,
          useValue: {},
        },
        {
          provide: getModelToken(User.name),
          useValue: mongoose.model(User.name, UserSchema),
        },
        {
          provide: getModelToken(Token.name),
          useValue: mongoose.model(Token.name, TokenSchema),
        },
        {
          provide: getModelToken(Favorite.name),
          useValue: mongoose.model(Favorite.name, FavoriteSchema),
        },
      ],
    }).compile();

    controller = module.get<MeController>(MeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
