import {
  Body,
  ConflictException,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Injectable,
  Param,
  Post,
  Put,
  Req,
  UnauthorizedException,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Request } from 'express';
import { I18nLang } from 'nestjs-i18n';
import { RoleType } from '../../constant';
import { PromiseJSONResponse } from '../../utils/types';
import { Roles } from '../auth/decorators/role.decorator';
import { UserInfo } from '../auth/decorators/user.decorator';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { UserPayload } from '../auth/interfaces/user-payload';
import { MangaDto } from '../manga/dto/manga.dto';
import { Manga, toMangaDto } from '../manga/schemas/manga.schema';
import { AddToHistoryDto } from '../user/dto/add-to-history.dto';
import { UpdateUserDto } from '../user/dto/update-user.dto';
import { ErrEmailExisted, ErrWrongPassword } from '../user/errors';
import { User } from '../user/schemas/user.schema';
import { UserService } from '../user/user.service';
import { AddFavoriteDto } from './dto/add-favorite.dto';
import { ChangePasswordDto } from './dto/change-password.dto';
import { RemoveFavoriteDto } from './dto/remove-favorite.dto';
import { ProfileInterceptor } from './interceptor/profile.interceptor';

@Controller('me')
@UseGuards(JwtAuthGuard)
@Injectable()
export class MeController {
  constructor(private userService: UserService) {}
  @Get('profile')
  @UseInterceptors(ProfileInterceptor)
  @Roles(RoleType.USER)
  getProfile(@UserInfo() user: UserPayload): Promise<User | null> {
    return this.userService.findProfileById(user.sub);
  }

  @Put('password')
  @UseInterceptors(ProfileInterceptor)
  updatePassword(
    @UserInfo() user: UserPayload,
    @Body() dto: ChangePasswordDto,
  ): Promise<User | null> {
    return this.userService.updatePasswordById(user.sub, dto).catch((err) => {
      if (err === ErrWrongPassword) {
        throw new UnauthorizedException(err.message);
      }

      throw err;
    });
  }

  @Put('profile')
  @UseInterceptors(ProfileInterceptor)
  @Roles(RoleType.USER)
  updateProfile(
    @UserInfo() user: UserPayload,
    @Body() dto: UpdateUserDto,
  ): Promise<User | null> {
    return this.userService.updateProfileById(user.sub, dto).catch((err) => {
      if (err === ErrEmailExisted) {
        throw new ConflictException(err.message);
      }

      throw err;
    });
  }

  @Get('favorite')
  @Roles(RoleType.USER)
  getFavorite(
    @Req() req: Request,
    @I18nLang() lang: string,
  ): PromiseJSONResponse<MangaDto[]> {
    const user = req.user as UserPayload;
    return this.userService.findFavoriteByUserId(user.sub).then((mangas) => {
      return {
        data: mangas.map((item) => toMangaDto(item, lang)),
      };
    });
  }

  @Post('favorite')
  @Put('favorite')
  @HttpCode(HttpStatus.ACCEPTED)
  @Roles(RoleType.USER)
  upsertFavorite(
    @UserInfo() user: UserPayload,
    @Body() dto: AddFavoriteDto,
    @I18nLang() lang: string,
  ): PromiseJSONResponse<MangaDto[]> {
    return this.userService
      .addNewFavorite(user.sub, dto.manga)
      .then((favorite) => {
        return {
          data: favorite.map((item) => toMangaDto(item, lang)),
        };
      });
  }

  @Delete('favorite')
  @HttpCode(HttpStatus.ACCEPTED)
  @Roles(RoleType.USER)
  removeFavorite(
    @UserInfo() user: UserPayload,
    @Body() dto: RemoveFavoriteDto,
    @I18nLang() lang: string,
  ): PromiseJSONResponse<MangaDto[]> {
    return this.userService
      .removeFavorite(user.sub, dto.manga)
      .then((favorite) => {
        return {
          data: favorite.map((item) => toMangaDto(item, lang)),
        };
      });
  }

  @Get('favorite/:mangaId/status')
  @Roles(RoleType.USER)
  checkFavorite(
    @UserInfo() user: UserPayload,
    @Param('mangaId') mangaId: string,
  ): PromiseJSONResponse<boolean> {
    return this.userService
      .isFavorite(user.sub, mangaId)
      .then((res) => ({ data: res }));
  }

  @Post('history')
  @Roles(RoleType.USER)
  addToHistory(
    @Body() dto: AddToHistoryDto,
    @UserInfo() user: UserPayload,
  ): Promise<void> {
    return this.userService.addToHistory(user.sub, dto.chapterId);
  }

  @Get('history')
  @Roles(RoleType.USER)
  getHistory(
    @I18nLang() langCode: string,
    @UserInfo() user: UserPayload,
  ): PromiseJSONResponse<MangaDto[]> {
    return this.userService
      .getHistory(user.sub)
      .then((res) => {
        console.log(res);
        return res;
      })
      .then((res) => ({
        data: res.map((item) => toMangaDto(item, langCode)),
      }));
  }

  @Get('history/chapters')
  @Roles(RoleType.USER)
  getHistoryAsChapters(
    @I18nLang() langCode: string,
    @UserInfo() user: UserPayload,
  ) {
    return this.userService
      .getHistoryAsChapters(user.sub, langCode)
      .then((res) => {
        return {
          data: res,
        };
      });
  }
}
