import { Module } from '@nestjs/common';
import { MeController } from './me.controller';
import { UserModule } from '../user/user.module';
import { UserService } from '../user/user.service';

@Module({
  imports: [UserModule],
  controllers: [MeController],
  providers: [UserService],
})
export class MeModule {}
