import { IsMongoId } from 'class-validator';

export class RemoveFavoriteDto {
  @IsMongoId()
  manga: string;
}
