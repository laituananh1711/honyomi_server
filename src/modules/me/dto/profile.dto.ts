import { User } from '../../user/schemas/user.schema';
import { RoleType } from '../../../constant';

class SettingDto {
  language: string;
}

export class ProfileDto {
  constructor(user: User) {
    this.id = user.id;
    this.username = user.username;
    this.email = user.email;
    this.role = user.role;
    this.avatarUrl = user.avatarUrl;
    this.setting = user.setting;
  }

  id: string;
  username: string;
  email: string;
  role: RoleType;
  avatarUrl: string;
  setting: SettingDto;
}
