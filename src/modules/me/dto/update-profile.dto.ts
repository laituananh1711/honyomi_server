import {
  IsEmail,
  IsIn,
  IsOptional,
  IsString,
  IsUrl,
  Length,
  ValidateNested,
} from 'class-validator';
import { PASSWORD_LENGTH } from '../../../constant';
import { Type } from 'class-transformer';
import { languageCodes } from '../../../assets/lang';

class SettingDto {
  @IsOptional()
  @IsIn(languageCodes)
  language: string;
}

export class UpdateProfileDto {
  @IsString()
  @Length(PASSWORD_LENGTH.min, PASSWORD_LENGTH.max)
  @IsOptional()
  password?: string;

  @ValidateNested()
  @Type(() => SettingDto)
  @IsOptional()
  setting?: SettingDto;

  @IsEmail()
  @IsOptional()
  email?: string;

  @IsUrl()
  @IsOptional()
  avatarUrl?: string;
}
