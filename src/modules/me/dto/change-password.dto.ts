import { IsString, Length } from 'class-validator';
import { PASSWORD_LENGTH } from '../../../constant';

export class ChangePasswordDto {
  @IsString()
  currentPassword: string;

  @IsString()
  @Length(PASSWORD_LENGTH.min, PASSWORD_LENGTH.max)
  newPassword: string;
}
