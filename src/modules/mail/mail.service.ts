import { MailerService } from '@nestjs-modules/mailer';
import { Injectable, Logger } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { events } from '../../constant';
import { ChapterCreatedEvent } from '../manga/events';
import { MangaService } from '../manga/manga.service';
import { UserCreatedEvent } from '../user/events';
import { UserService } from '../user/user.service';

@Injectable()
export class MailService {
  constructor(
    private mailerService: MailerService,
    private userService: UserService,
    private mangaService: MangaService,
  ) {}

  @OnEvent(events.USER_CREATED)
  handleUserCreatedEvent(payload: UserCreatedEvent): void {
    this.mailerService
      .sendMail({
        to: payload.email,
        subject: 'Welcome to Honyomi!',
        template: './welcome',
        context: {
          name: payload.username,
        },
      })
      .then((r) => Logger.log(r))
      .catch((e) => Logger.error(e));
  }

  @OnEvent(events.CHAPTER_CREATED)
  async handleChapterCreatedEvent(payload: ChapterCreatedEvent): Promise<void> {
    const mailingList = await this.getNewChapterMailingList(payload.mangaId);
    if (mailingList.length === 0) {
      Logger.debug('Mailing list is empty');
      return;
    }
    Logger.debug(mailingList);

    const manga = await this.mangaService.findOne(payload.mangaId);

    if (manga == null) {
      Logger.error(`manga with id ${payload.mangaId} not found`);
      return;
    }

    const { canonicalTitle } = manga;

    this.mailerService
      .sendMail({
        to: mailingList,
        subject: `${canonicalTitle}: New chapter`,
        template: './new_chapter',
        context: {
          mangaName: canonicalTitle,
          mangaLink: `https://laituananh1711.gitlab.io/honyomi_client/reader/${payload.mangaId}/${payload.chapter.number}/`,
        },
      })
      .catch(Logger.error);
  }

  private async getNewChapterMailingList(mangaId: string): Promise<string[]> {
    const userIds = await this.userService.getLikedUsersByMangaId(mangaId);

    const mailingList: string[] = await this.userService
      .getNewChapterMailedUsers(userIds)
      .then((res) => res.map((item) => item.email));

    return mailingList;
  }
}
