import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { join } from 'path';
import { MangaModule } from '../manga/manga.module';
import { UserModule } from '../user/user.module';
import { MailService } from './mail.service';

@Module({
  imports: [
    MailerModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        const authUser = configService.get('MAIL_USER', '');
        const authPass = configService.get('MAIL_PASS', '');
        const mailService = configService.get('MAIL_SERVICE', 'gmail');
        return {
          transport: {
            service: mailService,
            auth: {
              user: authUser,
              pass: authPass,
            },
          },
          defaults: {
            from: '"No Reply" <noreply@honyomi.tk>',
          },
          template: {
            dir: join(__dirname, 'templates'),
            adapter: new HandlebarsAdapter(),
            options: {
              strict: true,
            },
          },
        };
      },
    }),
    UserModule,
    MangaModule,
  ],
  providers: [MailService],
  exports: [MailService],
})
export class MailModule {}
