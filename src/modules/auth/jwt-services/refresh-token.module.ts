import { Module } from '@nestjs/common';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { ConfigModule, ConfigService, getConfigToken } from '@nestjs/config';
import { REFRESH_TOKEN_TIME } from '../../../constant';

@Module({
  imports: [
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => {
        return {
          secret: configService.get<string>(
            'REFRESH_TOKEN_SECRET',
            'refresh-secret',
          ),
          signOptions: {
            expiresIn: REFRESH_TOKEN_TIME,
          },
        };
      },
      inject: [ConfigService],
    }),
  ],
  providers: [
    {
      provide: getConfigToken(RefreshTokenModule.name),
      useExisting: JwtService,
    },
  ],
  exports: [getConfigToken(RefreshTokenModule.name)],
})
export class RefreshTokenModule {}
