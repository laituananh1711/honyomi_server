import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService, getConfigToken } from '@nestjs/config';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { ACCESS_TOKEN_TIME } from '../../../constant';

@Module({
  imports: [
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => {
        return {
          secret: configService.get<string>(
            'ACCESS_TOKEN_SECRET',
            'accesstoken',
          ),
          signOptions: {
            expiresIn: ACCESS_TOKEN_TIME,
          },
        };
      },
      inject: [ConfigService],
    }),
  ],
  providers: [
    {
      provide: getConfigToken(AccessTokenModule.name),
      useExisting: JwtService,
    },
  ],
  exports: [getConfigToken(AccessTokenModule.name)],
})
export class AccessTokenModule {}
