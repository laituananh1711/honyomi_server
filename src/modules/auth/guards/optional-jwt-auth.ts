import { AuthGuard } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';

@Injectable()
export class OptionalJwtAuth extends AuthGuard('jwt') {
  handleRequest(err: any, user: any): any {
    return user;
  }
}
