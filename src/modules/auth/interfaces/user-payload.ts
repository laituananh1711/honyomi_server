import { RoleType } from '../../../constant';

export interface UserPayload {
  sub: string;
  username: string;
  role: RoleType;
}

export interface IdentifyPayload extends UserPayload {
  //Binding current access token with refresh token generated it
  rtiat: number;
  rtexp: number;
}
