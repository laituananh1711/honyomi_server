import { IdentifyPayload, UserPayload } from './user-payload';

export interface TokenPayload {
  iat: number;
  exp: number;
}

export interface RefreshTokenPayload extends UserPayload, TokenPayload {}

export interface AccessTokenPayload extends IdentifyPayload, TokenPayload {}
