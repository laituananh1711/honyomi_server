import { CACHE_MANAGER, Inject, Injectable, Logger } from '@nestjs/common';
import { getConfigToken } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Cache } from 'cache-manager';
import * as admin from 'firebase-admin';
import * as Mongoose from 'mongoose';
import { Model } from 'mongoose';
import { CreateUserDTO } from '../user/dto/create-user.dto';
import { User } from '../user/schemas/user.schema';
import { UserService } from '../user/user.service';
import { RegisterDto } from './dto/register.dto';
import {
  AccessTokenPayload,
  RefreshTokenPayload,
} from './interfaces/jwt-payload';
import { IdentifyPayload, UserPayload } from './interfaces/user-payload';
import { AccessTokenModule } from './jwt-services/access-token.module';
import { RefreshTokenModule } from './jwt-services/refresh-token.module';
import { Token, TokenDocument } from './schemas/token.schema';
import { auth } from '../../firebase';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    @Inject(getConfigToken(RefreshTokenModule.name))
    private refreshTokenService: JwtService,
    @Inject(getConfigToken(AccessTokenModule.name))
    private accessTokenService: JwtService,
    @InjectModel(Token.name)
    private tokenModel: Model<TokenDocument>,
    @Inject(CACHE_MANAGER)
    private cacheManager: Cache,
  ) {}

  register(dto: RegisterDto): Promise<User> {
    return this.userService.createUser(dto as CreateUserDTO);
  }

  validateCredentials(
    username: string,
    password: string,
  ): Promise<User | null> {
    return this.userService
      .findByUsername(username)
      .then((user: User | null) => {
        if (user === null || !user.validatePassword(password)) {
          return null;
        } else {
          return user;
        }
      });
  }

  createRefreshToken(user: User): Promise<string> {
    const payload: UserPayload = {
      sub: user.id,
      username: user.username,
      role: user.role,
    };

    return this.refreshTokenService
      .signAsync(payload)
      .then((token) => {
        return {
          token: token,
          refreshTokenPayload: this.refreshTokenService.decode(
            token,
          ) as RefreshTokenPayload,
        };
      })
      .then(({ token, refreshTokenPayload }) => {
        return this.tokenModel
          .findOneAndUpdate(
            {
              user: new Mongoose.Types.ObjectId(refreshTokenPayload.sub),
            },
            {
              $addToSet: {
                tokens: {
                  iat: refreshTokenPayload.iat,
                  exp: refreshTokenPayload.exp,
                },
              },
            },
            {
              new: true,
              upsert: true,
            },
          )
          .then(() => {
            return this.removeExpirationToken(user.id, refreshTokenPayload.iat);
          })
          .then(() => {
            return token;
          });
      })
      .then((token) => token);
  }

  validateRefreshToken(
    refreshToken: string,
  ): Promise<RefreshTokenPayload | null> {
    return this.refreshTokenService
      .verifyAsync(refreshToken)
      .then((payload: RefreshTokenPayload) => {
        return this.tokenModel
          .findOne({
            user: new Mongoose.Types.ObjectId(payload.sub),
            tokens: {
              $elemMatch: {
                iat: payload.iat,
                exp: payload.exp,
              },
            },
          })
          .then((doc) => {
            if (!doc) {
              return null;
            }
            return payload;
          });
      });
  }

  createAccessToken(identifyPayload: IdentifyPayload): Promise<string> {
    return this.accessTokenService.signAsync(identifyPayload);
  }

  /**
   * Create a Firebase custom token.
   * Client should use this token to authenticate with Firebase.
   * This token does NOT need to be refreshed on the server side.
   * Client will use firebase SDK to implement this feature.
   * https://firebase.google.com/docs/auth/admin/create-custom-tokens
   * @param payload user payload
   * @returns firebase custom token
   */
  createFirebaseAccessToken(payload: UserPayload): Promise<string> {
    return admin.auth().createCustomToken(payload.sub, {
      username: payload.username,
      role: payload.role,
    });
  }

  validateAccessToken(
    accessToken: string,
  ): Promise<RefreshTokenPayload | null> {
    return this.accessTokenService
      .verifyAsync(accessToken)
      .then((payload: RefreshTokenPayload) => {
        return this.isValidToken(payload).then((isValid) => {
          if (isValid) {
            return payload as RefreshTokenPayload;
          }
          return null;
        });
      });
  }

  isValidToken(payload: RefreshTokenPayload): Promise<boolean> {
    return this.cacheManager.get<number>(payload.sub).then((exp) => {
      return exp == null || exp !== payload.exp;
    });
  }

  invalidAccessToken(accessTokenPayload: AccessTokenPayload): Promise<number> {
    return this.cacheManager.set<number>(
      accessTokenPayload.sub,
      accessTokenPayload.exp,
    );
  }

  logout(accessTokenPayload: AccessTokenPayload) {
    return this.tokenModel
      .updateMany(
        {
          user: new Mongoose.Types.ObjectId(accessTokenPayload.sub),
        },
        {
          $pull: {
            tokens: {
              iat: accessTokenPayload.rtiat,
              exp: accessTokenPayload.rtexp,
            },
          },
        },
      )
      .then(() => {
        return this.invalidAccessToken(accessTokenPayload);
      });
  }

  removeExpirationToken(userId: string, timeNow: number): Promise<void> {
    return this.tokenModel
      .updateMany(
        {
          user: new Mongoose.Types.ObjectId(userId),
        },
        {
          $pull: {
            tokens: {
              exp: { $lt: timeNow },
            },
          },
        },
      )
      .then((r) => {
        Logger.log(r);
      });
  }

  verifyFirebaseAccessToken(idToken: string): Promise<string | undefined> {
    return auth.verifyIdToken(idToken).then((token) => {
      return token.email;
    });
  }

  async getAllToken(user: User): Promise<{
    firebaseAccessToken: string;
    accessToken: string;
    refreshToken: string;
  }> {
    const userPayload: UserPayload = {
      username: user.username,
      sub: user.id,
      role: user.role,
    };

    const getRefreshToken = this.createRefreshToken(user);
    const getFirebaseAccessToken = this.createFirebaseAccessToken(userPayload);

    return Promise.all([getRefreshToken, getFirebaseAccessToken]).then(
      async (res) => {
        const [refreshToken, firebaseAccessToken] = res;

        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const refreshPayload = (await this.validateRefreshToken(refreshToken))!;

        const accessToken = await this.createAccessToken({
          sub: refreshPayload.sub,
          username: refreshPayload.username,
          role: refreshPayload.role,
          rtiat: refreshPayload.iat,
          rtexp: refreshPayload.exp,
        });

        return { refreshToken, accessToken, firebaseAccessToken };
      },
    );
  }
}
