import { IsNotEmpty, IsString } from 'class-validator';

//TODO: Can login with username either email
export class LoginDto {
  @IsString()
  @IsNotEmpty()
  username: string;

  @IsString()
  @IsNotEmpty()
  password: string;
}
