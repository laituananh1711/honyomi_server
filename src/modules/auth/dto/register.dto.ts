import { IsEmail, IsString, Length } from 'class-validator';
import { PASSWORD_LENGTH, USERNAME_LENGTH } from '../../../constant';

export class RegisterDto {
  @IsString()
  @Length(USERNAME_LENGTH.min, USERNAME_LENGTH.max)
  readonly username: string;

  @IsString()
  @Length(PASSWORD_LENGTH.min, PASSWORD_LENGTH.max)
  password: string;

  @IsEmail()
  readonly email: string;
}
