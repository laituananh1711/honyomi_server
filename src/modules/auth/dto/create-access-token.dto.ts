import { IsJWT } from 'class-validator';

export class CreateAccessTokenDto {
  @IsJWT()
  refreshToken: string;
}
