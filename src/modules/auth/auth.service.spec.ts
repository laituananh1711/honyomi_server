import { CACHE_MANAGER } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import * as mongoose from 'mongoose';
import { UserService } from '../user/user.service';
import { AuthService } from './auth.service';
import { AccessTokenModule } from './jwt-services/access-token.module';
import { RefreshTokenModule } from './jwt-services/refresh-token.module';
import { Token, TokenSchema } from './schemas/token.schema';

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AccessTokenModule, RefreshTokenModule],
      providers: [
        AuthService,
        {
          provide: getModelToken(Token.name),
          useValue: mongoose.model(Token.name, TokenSchema),
        },
        {
          provide: UserService,
          useValue: {},
        },
        {
          provide: CACHE_MANAGER,
          useValue: () => ({}),
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
