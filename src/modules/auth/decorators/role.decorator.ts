import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { RoleType } from '../../../constant';
import { JwtAuthGuard } from '../guards/jwt-auth.guard';
import { RoleGuard } from '../guards/role.guard';

export const ROLES_KEY = 'roles';
export function Roles(...roles: RoleType[]) {
  return applyDecorators(
    SetMetadata(ROLES_KEY, roles),
    UseGuards(JwtAuthGuard, RoleGuard),
  );
}
