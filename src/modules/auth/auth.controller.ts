import {
  BadRequestException,
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  Req,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { UserService } from '../user/user.service';
import { AuthService } from './auth.service';
import { CreateAccessTokenDto } from './dto/create-access-token.dto';
import { LoginDto } from './dto/login.dto';
import { RegisterDto } from './dto/register.dto';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { AccessTokenPayload } from './interfaces/jwt-payload';
import { IdentifyPayload, UserPayload } from './interfaces/user-payload';
import { PromiseJSONResponse } from '../../utils/types';
import { ProfileDto } from '../me/dto/profile.dto';
import { UserInfo } from './decorators/user.decorator';
import { FirebaseLoginDto } from './dto/firebase-login.dto';
import { User } from '../user/schemas/user.schema';

@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private userService: UserService,
  ) {}

  @Post('register')
  @HttpCode(HttpStatus.CREATED)
  register(@Body() dto: RegisterDto): PromiseJSONResponse {
    return this.userService
      .findByUsername(dto.username)
      .then((user) => {
        if (user) {
          throw new BadRequestException('errors.usernameExist');
        }
      })
      .then(() => {
        return this.authService.register(dto);
      })
      .then(() => ({
        data: {},
        meta: {
          message: 'Register success',
        },
      }))
      .catch((e) => {
        throw new BadRequestException(e);
      });
  }

  async generateLoginResponse(user: User): Promise<{
    firebaseAccessToken: string;
    accessToken: string;
    user: ProfileDto;
    refreshToken: string;
  }> {
    return this.authService.getAllToken(user).then((res) => ({
      ...res,
      user: new ProfileDto(user),
    }));
  }

  @Post('login')
  async login(@Body() dto: LoginDto): PromiseJSONResponse<{
    refreshToken: string;
    accessToken: string;
    firebaseAccessToken: string;
    user: ProfileDto;
  }> {
    const user = await this.authService.validateCredentials(
      dto.username,
      dto.password,
    );

    if (!user) {
      throw new UnauthorizedException('errors.wrongCredentials');
    }

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete user.password;

    return this.generateLoginResponse(user)
      .then((payload) => ({ data: payload }))
      .catch((e) => {
        throw new BadRequestException(e);
      });
  }

  @Post('access')
  @HttpCode(HttpStatus.OK)
  createAccessToken(
    @Body() dto: CreateAccessTokenDto,
  ): PromiseJSONResponse<{ accessToken: string; firebaseAccessToken: string }> {
    return this.authService
      .validateRefreshToken(dto.refreshToken)
      .then((refreshTokenPayload): IdentifyPayload => {
        if (!refreshTokenPayload) {
          throw new BadRequestException('errors.wrongRefreshToken');
        }
        return {
          sub: refreshTokenPayload.sub,
          username: refreshTokenPayload.username,
          role: refreshTokenPayload.role,
          rtiat: refreshTokenPayload.iat,
          rtexp: refreshTokenPayload.exp,
        };
      })
      .then((payload) => {
        return Promise.all([
          this.authService.createAccessToken(payload),
          this.authService.createFirebaseAccessToken(payload),
        ]);
      })
      .then(([accessToken, firebaseAccessToken]) => {
        return { data: { accessToken, firebaseAccessToken } };
      })
      .catch((e) => {
        //Handle jwt expired or wrong jwt from Auth and Firebase
        throw new BadRequestException(e);
      });
  }

  @UseGuards(JwtAuthGuard)
  @Post('logout')
  logout(@UserInfo() user: UserPayload) {
    return this.authService
      .logout(user as AccessTokenPayload)
      .then(() => ({ meta: { message: 'Logout success' } }))
      .catch((e) => {
        throw new BadRequestException(e);
      });
  }

  @Post('firebase')
  async loginWithFirebase(@Body() dto: FirebaseLoginDto) {
    const email = await this.authService.verifyFirebaseAccessToken(dto.idToken);

    if (!email) {
      throw new UnauthorizedException('errors.wrongToken');
    }

    const user = await this.userService.findUserByEmail(email);

    if (!user) {
      throw new BadRequestException('errors.emailNotMatchAnyUser');
    }

    return this.generateLoginResponse(user)
      .then((payload) => ({ data: payload }))
      .catch((e) => {
        throw new BadRequestException(e);
      });
  }
}
