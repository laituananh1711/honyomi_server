import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { User } from '../../user/schemas/user.schema';
import * as Mongoose from 'mongoose';

export type TokenDocument = Token & Document;

@Schema({ _id: false })
export class TokenIdentifier extends Document {
  //iat(issued at) and exp(expiration time) with jwt signature is enough to
  // detect token are valid
  iat: number;
  exp: number;
}
export const TokenIdentifierSchema =
  SchemaFactory.createForClass(TokenIdentifier);

//Only for refresh token, access token never save in database
@Schema()
export class Token {
  //TODO: using userId and user field to clear naming
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: User.name })
  user: User | Mongoose.Types.ObjectId;

  @Prop({})
  tokens: TokenIdentifier[];
}

export const TokenSchema = SchemaFactory.createForClass(Token);
