import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Injectable } from '@nestjs/common';
import { AuthService } from '../auth.service';
import { ConfigService } from '@nestjs/config';
import { RefreshTokenPayload } from '../interfaces/jwt-payload';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    private authService: AuthService,
    private configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get<string>(
        'ACCESS_TOKEN_SECRET',
        'accesssecret',
      ),
    });
  }

  async validate(payload: any): Promise<RefreshTokenPayload | null> {
    return this.authService.isValidToken(payload).then((isValid) => {
      if (isValid) {
        return payload as RefreshTokenPayload;
      }
      return null;
    });
  }
}
