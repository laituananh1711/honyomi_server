import {
  Body,
  Controller,
  Get,
  Injectable,
  InternalServerErrorException,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { PromiseJSONResponse } from '../../utils/types';
import { Comment } from './schemas/comment.schema';
import { CommentService } from './comment.service';
import { ErrCommentNotFound } from './errors';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { UserInfo } from '../auth/decorators/user.decorator';
import { UserPayload } from '../auth/interfaces/user-payload';
import { ReplyCommentDto } from './dto/reply-comment.dto';
import { CommentQuery } from './dto/comment-query';
import { parseSortQuery } from '../../utils/parse-sort-query';

@Injectable()
@Controller('comments')
export class CommentController {
  constructor(private commentService: CommentService) {}

  @Get(':id/replies')
  async getComment(
    @Param('id') id: string,
    @Query() { sort }: CommentQuery,
  ): PromiseJSONResponse<Comment[]> {
    const allowSortColumn = ['repliesCount', 'createdAt'];
    const comment = await this.commentService.getComment(
      id,
      parseSortQuery(sort ?? [], { allowedCols: allowSortColumn }),
    );
    if (!comment) {
      throw ErrCommentNotFound;
    }
    return {
      data: comment.replies as Comment[],
    };
  }

  @UseGuards(JwtAuthGuard)
  @Post(':id/replies')
  async createReplies(
    @Param('id') id: string,
    @UserInfo() user: UserPayload,
    @Body() dto: ReplyCommentDto,
  ): PromiseJSONResponse<Comment> {
    return this.commentService.replyComment(id, user.sub, dto).then((reply) => {
      if (!reply) {
        throw new InternalServerErrorException('something went wrong');
      }
      return { data: reply };
    });
  }
}
