import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema, Types } from 'mongoose';
import { User } from '../../user/schemas/user.schema';
import { Chapter } from '../../manga/schemas/chapter.schema';

export type CommentDocument = Comment & Document;

@Schema({
  timestamps: true,
})
export class Comment {
  @Prop()
  comment: string;

  @Prop({ type: MongooseSchema.Types.ObjectId, ref: User.name })
  user: User | Types.ObjectId;

  //Manga exists if only is root comment
  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: Chapter.name,
    required: false,
  })
  chapter?: Chapter | Types.ObjectId;

  @Prop({
    type: [{ type: MongooseSchema.Types.ObjectId, ref: Comment.name }],
    default: [],
  })
  replies: Comment[] | Types.ObjectId[];

  @Prop({ type: Number, default: 0 })
  repliesCount: number;
}

export const CommentSchema = SchemaFactory.createForClass(Comment);
