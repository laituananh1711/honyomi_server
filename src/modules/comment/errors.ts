import { NotFoundException } from '@nestjs/common';

export const ErrCommentNotFound = new NotFoundException(
  'errors.commentNotFound',
);
ErrCommentNotFound.name = 'ErrCommentNotFound';
