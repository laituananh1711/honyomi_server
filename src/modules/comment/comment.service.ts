import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Comment, CommentDocument } from './schemas/comment.schema';
import { Model } from 'mongoose';
import { CreateCommentDto } from '../manga/dto/create-comment.dto';
import { ReplyCommentDto } from './dto/reply-comment.dto';
import { SortOptions } from '../../utils/parse-sort-query';

@Injectable()
export class CommentService {
  static userProjectFields = 'id avatarUrl username';

  constructor(
    @InjectModel(Comment.name)
    private readonly commentModel: Model<CommentDocument>,
  ) {}

  //TODO: add order and paginate
  getComments(chapterId: string, sort?: SortOptions): Promise<Comment[]> {
    let query = this.commentModel.find(
      {
        chapter: chapterId,
      },
      {
        chapter: 0,
        replies: 0,
      },
      {
        populate: {
          path: 'user',
          select: CommentService.userProjectFields,
        },
      },
    );

    if (sort) {
      query = query.sort(sort);
    }

    return query.exec();
  }

  getComment(commentId: string, sort?: SortOptions): Promise<Comment | null> {
    return this.commentModel
      .findById(commentId)
      .populate({
        path: 'replies',
        select: 'id comment repliesCount createdAt updatedAt user',
        populate: {
          path: 'user',
          select: CommentService.userProjectFields,
        },
        options: {
          sort: sort ?? '',
        },
      })
      .populate('user', CommentService.userProjectFields)
      .exec();
  }

  createComment(
    chapterId: string,
    userId: string,
    dto: CreateCommentDto,
  ): Promise<Comment> {
    return this.commentModel
      .create({
        chapter: chapterId,
        user: userId,
        ...dto,
      })
      .catch((e) => {
        throw e;
      });
  }

  async replyComment(
    commentId: string,
    userId: string,
    dto: ReplyCommentDto,
  ): Promise<Comment | null> {
    const reply = await this.commentModel.create({
      user: userId,
      ...dto,
    });

    await this.commentModel
      .findByIdAndUpdate(
        commentId,
        {
          $push: {
            replies: reply,
          },
          $inc: {
            repliesCount: 1,
          },
        },
        {
          upsert: true,
          new: true,
          setDefaultsOnInsert: true,
          populate: [
            {
              path: 'replies',
            },
            {
              path: 'user',
              select: CommentService.userProjectFields,
            },
          ],
        },
      )
      .exec();

    return reply;
  }

  countComments(chapterId: string): Promise<number> {
    return this.commentModel
      .countDocuments({
        chapter: chapterId,
      })
      .exec();
  }
}
