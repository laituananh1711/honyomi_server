import { IsOptional, IsString } from 'class-validator';

export class CommentQuery {
  @IsString({ each: true })
  @IsOptional()
  sort?: string[];
}
