import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as SchemaMongoose, Types } from 'mongoose';
import { ChapterDto } from '../dto/chapter.dto';
import { MangaDto } from '../dto/manga.dto';
import { Manga, toMangaDto } from './manga.schema';
import { LanguageCodeToValue, titleValidationFn } from './title.schema';

export type ChapterDocument = Chapter & Document;

@Schema()
export class Chapter {
  @Prop({
    type: SchemaMongoose.Types.Map,
    of: String,
    validate: titleValidationFn,
    default: {},
  })
  titles: LanguageCodeToValue;

  @Prop({ type: String, required: true, default: '' })
  canonicalTitle: string;

  @Prop({ required: true })
  translatedLanguage: string;

  @Prop({ type: [String], default: [] })
  pages: string[];

  @Prop({ required: true })
  thumbnailUrl: string; // TODO: make optional

  @Prop({ type: SchemaMongoose.Types.ObjectId, ref: 'Manga' })
  manga: Manga | Types.ObjectId;

  @Prop({ required: true })
  number: string;

  @Prop({ default: 0 })
  viewCount: number; // TODO: rethink use case
}

export const ChapterSchema = SchemaFactory.createForClass(Chapter);

export function toChapterDto(chapter: Chapter, langCode: string): ChapterDto {
  const { titles, translatedLanguage, pages, thumbnailUrl, manga, number } =
    chapter;

  let title;
  if (titles) {
    if (titles instanceof Map) {
      title = titles.get(langCode);
    } else {
      title = titles[langCode];
    }
  }

  let mangaDto: MangaDto | string;
  if (typeof manga === 'string' || manga instanceof Types.ObjectId) {
    mangaDto = manga.toString();
  } else {
    mangaDto = toMangaDto(manga as any, langCode);
  }

  return {
    id: (chapter as any).id.toString(),
    title: title ?? chapter.canonicalTitle,
    translatedLanguage,
    pages,
    thumbnailUrl,
    manga: mangaDto,
    number,
  };
}
