import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { MangaDto } from '../dto/manga.dto';
import { RecommendedMangaDto } from '../dto/recommended-manga.dto';
import { Manga, toMangaDto } from './manga.schema';

export type RecommendedMangaDocument = RecommendedManga & Document;

@Schema({ collection: 'recommended_mangas' })
export class RecommendedManga {
  @Prop({ type: String, required: true })
  titleColor: string;

  @Prop({ type: String, default: '' })
  description: string;

  @Prop({ type: Types.ObjectId, required: true, ref: Manga.name, unique: true })
  manga: Manga | Types.ObjectId;

  @Prop({ type: Boolean, default: false })
  isPublished: boolean;

  @Prop({ type: String, required: true, default: null })
  imageUrl: string;

  @Prop({ type: Date, default: Date.now })
  createdAt: Date;
}

export const RecommendedMangaSchema =
  SchemaFactory.createForClass(RecommendedManga);

export function toRecommendedMangaDto(
  recommendedManga: RecommendedManga,
  langCode: string,
): RecommendedMangaDto {
  let mangaDto: MangaDto | string;
  if (recommendedManga.manga instanceof Types.ObjectId) {
    mangaDto = recommendedManga.manga._id.toString();
  } else {
    mangaDto = toMangaDto(recommendedManga.manga, langCode);
  }
  return {
    id: (recommendedManga as any)?.id,
    createdAt: recommendedManga.createdAt,
    description: recommendedManga.description,
    imageUrl: recommendedManga.imageUrl,
    isPublished: recommendedManga.isPublished,
    titleColor: recommendedManga.titleColor,
    manga: mangaDto,
  };
}
