import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId, Schema as SchemaMongoose } from 'mongoose';
import { AgeRating, MangaStatus, MangaType } from '../../../constant';
import { Author } from '../../author/schemas/author.schema';
import { MangaDto } from '../dto/manga.dto';
import { Category } from './category.schema';
import { Chapter, toChapterDto } from './chapter.schema';
import { CoverImage, CoverImageSchema } from './cover-image.schema';
import { LanguageCodeToValue, titleValidationFn } from './title.schema';

type MangaDocument = Manga & Document;

@Schema({ toJSON: { virtuals: true } })
class Manga {
  @Prop({
    type: SchemaMongoose.Types.Map,
    of: String,
    validate: titleValidationFn,
    required: true,
  })
  titles: LanguageCodeToValue;

  @Prop({ required: true })
  romajiTitle: string;

  @Prop({ required: true })
  canonicalTitle: string;

  @Prop({ default: '' })
  synopsis: string;

  /**
   * Should denormalize relation, write/read rate ~ 0.
   * */
  @Prop({ type: [{ type: SchemaMongoose.Types.ObjectId, ref: Author.name }] })
  authors: Author[];

  @Prop({ type: CoverImageSchema })
  coverImage: CoverImage;

  @Prop({
    enum: Object.values(MangaType),
    default: MangaType.MANGA,
  })
  mangaType: MangaType;

  @Prop({
    enum: Object.values(AgeRating),
    required: true,
  })
  ageRating: AgeRating;

  @Prop({
    enum: Object.values(MangaStatus),
    default: MangaStatus.ONGOING,
  })
  status: MangaStatus;

  @Prop({ type: [{ type: SchemaMongoose.Types.ObjectId, ref: 'Category' }] })
  categories: Category[] | ObjectId[];

  @Prop({ default: Date.now })
  createdAt: Date;

  chapters?: Chapter[];
}

const MangaSchema = SchemaFactory.createForClass(Manga);

MangaSchema.virtual('chapters', {
  ref: 'Chapter', // use literal string to avoid circular imports with Chapter
  localField: '_id',
  foreignField: 'manga',
});

/**
 * Transform a Mongo Manga into a Manga DTO.
 * @param manga input manga mongo object.
 * @param langCode the language code to which we will translate `title` to.
 * @returns manga dto.
 */
function toMangaDto(manga: Manga, langCode: string): MangaDto {
  let title;
  if (manga.titles) {
    if (manga.titles instanceof Map) {
      title = manga.titles.get(langCode);
    } else {
      title = manga.titles[langCode];
    }
  }

  return {
    id: (manga as any)?.id,
    title: title ?? manga.canonicalTitle,
    mangaType: manga.mangaType,
    romajiTitle: manga.romajiTitle,
    status: manga.status,
    authors: manga.authors,
    categories: manga.categories as Category[],
    synopsis: manga.synopsis,
    ageRating: manga.ageRating,
    coverImage: manga.coverImage,
    createdAt: manga.createdAt,
    chapters: manga.chapters?.map((item) => toChapterDto(item, langCode)),
  };
}

export { MangaDocument, Manga, MangaSchema, toMangaDto };
