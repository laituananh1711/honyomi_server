import { languageCodeToName } from '../../../assets/lang';
import { ErrInvalidLanguageCode } from '../errors';

export type LanguageCodeToValue = Map<string, string>;

export const titleValidationFn = (map: LanguageCodeToValue) => {
  for (const key of map.keys()) {
    if (languageCodeToName[key] == null) {
      throw ErrInvalidLanguageCode;
    }
  }
  return true;
};
