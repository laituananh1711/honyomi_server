import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type CategoryDocument = Category & Document;

@Schema({
  timestamps: true,
})
export class Category {
  @Prop({ required: true })
  title: string; // TODO: add multiple languages and canonicalTitle

  @Prop({ default: '' })
  description: string;

  @Prop({ default: false })
  nsfw: boolean;
}

export const CategorySchema = SchemaFactory.createForClass(Category);
