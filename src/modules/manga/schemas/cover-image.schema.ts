import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class CoverImageVariant {
  @Prop({ required: true })
  url: string;

  @Prop({ default: null })
  widthInPx: number;

  @Prop({ default: null })
  heightInPx: number;
}

export const CoverImageVariantSchema =
  SchemaFactory.createForClass(CoverImageVariant);

@Schema()
export class CoverImage {
  @Prop({ type: CoverImageVariantSchema, required: true })
  original: CoverImageVariant;

  // TODO: @anh.lt add images of different sizes and metadata if necessary.
}

export const CoverImageSchema = SchemaFactory.createForClass(CoverImage);
