import { Chapter } from './schemas/chapter.schema';

export class ChapterCreatedEvent {
  mangaId: string;
  chapter: Chapter;

  constructor(mangaId: string, chapter: Chapter) {
    this.mangaId = mangaId;
    this.chapter = chapter;
  }
}
