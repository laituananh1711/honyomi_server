import { NotFoundException } from '@nestjs/common';

const ErrInvalidLanguageCode = new Error('errors.invalidLangCode');
ErrInvalidLanguageCode.name = 'ErrInvalidLanguageCode';

const ErrMangaNotFound = new NotFoundException('errors.mangaNotFound');
ErrMangaNotFound.name = 'ErrMangaNotFound';

const ErrCategoryNotExisted = new Error('errors.categoryNotExisted');
ErrCategoryNotExisted.name = 'ErrCategoryNotExisted';

export { ErrInvalidLanguageCode, ErrMangaNotFound, ErrCategoryNotExisted };
