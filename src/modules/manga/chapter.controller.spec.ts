import { Test, TestingModule } from '@nestjs/testing';
import { ChapterController } from './chapter.controller';
import { CommentService } from '../comment/comment.service';

describe('ChapterController', () => {
  let controller: ChapterController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ChapterController],
      providers: [
        {
          provide: CommentService,
          useValue: {},
        },
      ],
    }).compile();

    controller = module.get<ChapterController>(ChapterController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
