import { EventEmitter2 } from '@nestjs/event-emitter';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import * as mongoose from 'mongoose';
import { MangaService } from './manga.service';
import { Chapter } from './schemas/chapter.schema';
import { Manga, MangaSchema } from './schemas/manga.schema';
import { RecommendedManga } from './schemas/recommended-manga.schema';

describe('MangaService', () => {
  let service: MangaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MangaService,
        {
          provide: getModelToken(Manga.name),
          useValue: mongoose.model(Manga.name, MangaSchema),
        },
        {
          provide: getModelToken(Chapter.name),
          useValue: {},
        },
        {
          provide: getModelToken(RecommendedManga.name),
          useValue: {},
        },
        {
          provide: EventEmitter2,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<MangaService>(MangaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
