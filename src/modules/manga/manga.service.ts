import { Injectable, Logger } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId, Types } from 'mongoose';
import { upperCaseFirstLetter } from '../../cli/utils';
import { events, MangaStatus } from '../../constant';
import { paginate, PaginatedResults } from '../../utils/paginate';
import { CreateChapterDto } from './dto/create-chapter.dto';
import { CreateMangaDto } from './dto/create-manga.dto';
import { MangaStatusDto } from './dto/manga.dto';
import {
  CreateRecommendedMangaDto,
  UpdateRecommendedMangaDto,
} from './dto/recommend-manga.dto';
import { ChapterCreatedEvent } from './events';
import { Chapter, ChapterDocument } from './schemas/chapter.schema';
import { Manga, MangaDocument } from './schemas/manga.schema';
import {
  RecommendedManga,
  RecommendedMangaDocument,
} from './schemas/recommended-manga.schema';
import {
  FindLatestChapterOptions,
  FindMangaOptions,
  FindRecommendedMangaOptions,
} from './types';

@Injectable()
export class MangaService {
  constructor(
    @InjectModel(Manga.name) private mangaModel: Model<MangaDocument>,
    @InjectModel(Chapter.name) private chapterModel: Model<ChapterDocument>,
    @InjectModel(RecommendedManga.name)
    private recommendedMangaModel: Model<RecommendedMangaDocument>,
    private eventEmitter: EventEmitter2,
  ) {}

  /**
   * Find all mangas.
   * This function accepts optional filter, sort and limit parameters.
   * @param findMangaOptions
   * @returns list of mangas which satisfied the input.
   */
  findAll({
    perPage,
    page,
    include,
    q,
    sort,
    filter,
  }: FindMangaOptions): Promise<PaginatedResults<Manga>> {
    let query = this.mangaModel.find(
      {},
      {
        titles: 1,
        romajiTitle: 1,
        canonicalTitle: 1,
        synopsis: 1,
        mangaType: 1,
        ageRating: 1,
        status: 1,
        createdAt: 1,
        coverImage: 1,
        categories: 1,
        authors: 1,
      },
    );

    if (q != null && q.length > 0) {
      query = query.find({
        $or: [
          {
            romajiTitle: new RegExp(q, 'i'),
          },
          {
            canonicalTitle: new RegExp(q, 'i'),
          },
        ],
      });
    }

    if (filter != null) {
      /* WORKAROUND */
      // For some god-knows-why reason, filter by authors only works
      // if we also filter by `status`.
      if (filter.status == null || filter.status.length === 0) {
        filter.status = [
          MangaStatus.ONGOING,
          MangaStatus.DROPPED,
          MangaStatus.FINISHED,
        ];
      }
      query = query.find({
        status: {
          $in: filter.status,
        },
      });
      /* END WORKAROUND */
      if (filter.authors != null && filter.authors.length > 0) {
        query = query.find({
          authors: {
            $in: filter.authors.map((item) => new Types.ObjectId(item)) as any,
          },
        });
      }
      if (filter.categories != null && filter.categories.length > 0) {
        query = query.find({
          categories: {
            $in: filter.categories.map(
              (item) => new Types.ObjectId(item),
            ) as any,
          },
        });
      }
    }

    if (include != null) {
      for (const incl of include) {
        if (
          incl === 'chapters' &&
          filter?.langCodes &&
          filter.langCodes.length > 0
        ) {
          query = query.populate({
            path: 'chapters',
            match: {
              translatedLanguage: {
                $in: filter.langCodes,
              },
            },
            select: 'number pages thumbnailUrl title translatedLanguage',
          });
          continue;
        }

        query = query.populate(incl);
      }
    }

    if (sort != null) {
      query = query.sort(sort);
    }

    return paginate(query, { perPage, page });
  }

  create(dto: CreateMangaDto): Promise<Manga> {
    const newManga = new this.mangaModel(dto);
    return newManga.save();
  }

  createChapter(mangaId: string, dto: CreateChapterDto): Promise<Chapter> {
    const newChapter = new this.chapterModel({
      ...dto,
      manga: mangaId,
    });
    return newChapter.save().then((res) => {
      this.eventEmitter.emit(
        events.CHAPTER_CREATED,
        new ChapterCreatedEvent(mangaId, res),
      );
      return res;
    });
  }

  deleteChapter(chapterId: string): Promise<Chapter | null> {
    return this.chapterModel.findByIdAndDelete(chapterId).exec();
  }

  findOne(id: string): Promise<Manga | null> {
    return this.mangaModel
      .findById(id)
      .populate('categories')
      .populate('chapters')
      .populate('authors')
      .exec();
  }

  findLatestChapters({
    limit,
    langCode = 'en',
  }: FindLatestChapterOptions): Promise<Chapter[]> {
    return this.chapterModel
      .find({
        translatedLanguage: langCode,
      })
      .sort({ createdAt: -1 })
      .limit(limit)
      .populate({ path: 'manga', select: 'canonicalTitle titles coverImage' })
      .exec();
  }

  createRecommendedManga(
    dto: CreateRecommendedMangaDto,
  ): Promise<RecommendedManga> {
    const newRecommendedManga = new this.recommendedMangaModel({
      titleColor: dto.titleColor,
      description: dto.description,
      manga: dto.mangaId,
      isPublished: dto.isPublished,
      imageUrl: dto.imageUrl,
    });
    return newRecommendedManga.save();
  }

  updateRecommendedManga(
    id: string,
    dto: UpdateRecommendedMangaDto,
  ): Promise<RecommendedManga | null> {
    return this.recommendedMangaModel
      .findByIdAndUpdate(id, dto, { new: true })
      .exec();
  }

  getRecommendedById(id: string): Promise<RecommendedManga | null> {
    return this.recommendedMangaModel.findById(id).exec();
  }

  findAllRecommended({
    filter,
  }: FindRecommendedMangaOptions): Promise<RecommendedManga[]> {
    let query = this.recommendedMangaModel.find(
      {},
      {
        titleColor: 1,
        description: 1,
        isPublished: 1,
        createdAt: 1,
        imageUrl: 1,
      },
    );

    if (filter != null) {
      const { isPublished } = filter;

      if (isPublished != null) {
        query = query.find({ isPublished });
      }
    }

    query = query.populate({
      path: 'manga',
      select: 'titles canonicalTitle categories romajiTitle status synopsis',
      populate: { path: 'categories' },
    });

    return query.exec();
  }

  // getMangaStatusWithCount ...
  async getMangaStatusWithCount(): Promise<MangaStatusDto[]> {
    const statusWithMangaCount: StatusWithMangaCount[] =
      await this.mangaModel.aggregate([
        {
          $group: {
            _id: '$status',
            count: { $sum: 1 },
          },
        },
      ]);
    return Object.values(MangaStatus).map((item: string) => {
      const statusIdx = statusWithMangaCount.findIndex(
        (status) => status['_id'] === item,
      );
      let mangaCount = 0;
      if (statusIdx >= 0) {
        mangaCount = statusWithMangaCount[statusIdx].count;
      }
      return {
        status: {
          name: upperCaseFirstLetter(item),
          value: item,
        },
        mangaCount,
      };
    });
  }

  // getMostPopularManga gets the most popular mangas
  // based on view count of its chapters.
  async getMostPopularManga(
    limit: number,
  ): Promise<{ manga: Manga; viewCount: number }[]> {
    const mangaViewCount: { _id: ObjectId; count: number }[] =
      await this.chapterModel
        .aggregate([
          {
            $group: { _id: '$manga', count: { $sum: '$viewCount' } },
          },
          {
            $sort: { count: -1 },
          },
          {
            $limit: limit,
          },
        ])
        .exec();

    const mangaIds = mangaViewCount.map((item) => item['_id'].toString());

    const mangas = await this.mangaModel
      .find({
        _id: {
          $in: mangaIds,
        },
      })
      .exec();

    return mangaViewCount.map((item) => ({
      manga: mangas.find(
        (manga) => manga.id.toString() === item['_id'].toString(),
      )!,
      viewCount: item.count,
    }));
  }
}

interface StatusWithMangaCount {
  _id: string;
  count: number;
}
