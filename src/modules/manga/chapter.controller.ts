import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { CommentService } from '../comment/comment.service';
import { PromiseJSONResponse } from '../../utils/types';
import { Comment } from '../comment/schemas/comment.schema';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { UserInfo } from '../auth/decorators/user.decorator';
import { UserPayload } from '../auth/interfaces/user-payload';
import { CreateCommentDto } from './dto/create-comment.dto';
import { CommentQuery } from '../comment/dto/comment-query';
import { parseSortQuery } from '../../utils/parse-sort-query';

@Controller('chapters')
export class ChapterController {
  constructor(private commentService: CommentService) {}

  @Get(':id/comments')
  getComments(
    @Param('id') id: string,
    @Query() { sort }: CommentQuery,
  ): PromiseJSONResponse<Comment[]> {
    const allowSortColumn = ['repliesCount', 'createdAt'];
    return this.commentService
      .getComments(
        id,
        parseSortQuery(sort ?? [], { allowedCols: allowSortColumn }),
      )
      .then((comments) => {
        return {
          data: comments,
        };
      });
  }

  @Get(':id/countComments')
  countComments(@Param('id') id: string): PromiseJSONResponse<number> {
    return this.commentService.countComments(id).then((count) => {
      return {
        data: count,
      };
    });
  }

  @UseGuards(JwtAuthGuard)
  @Post(':id/comments')
  createComment(
    @Param('id') id: string,
    @UserInfo() user: UserPayload,
    @Body() dto: CreateCommentDto,
  ): PromiseJSONResponse<Comment> {
    return this.commentService
      .createComment(id, user.sub, dto)
      .then((comment) => {
        return {
          data: comment,
        };
      });
  }
}
