import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { CategoryService } from './category.service';
import { PromiseJSONResponse } from '../../utils/types';
import { Category } from './schemas/category.schema';
import { RoleType } from '../../constant';
import { Roles } from '../auth/decorators/role.decorator';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { CategoryWithCountDto } from './dto/category-with-count.dto';
import { PaginatedResultsMeta } from '../../utils/paginate';
import { parseSortQuery } from '../../utils/parse-sort-query';
import { FindCategoryQuery } from './dto/category-query';
import { ErrCategoryNotExisted } from './errors';

@Controller('categories')
export class CategoryController {
  constructor(private categoryService: CategoryService) {}

  @Get()
  findAll(
    @Query() { perPage, page, filter, sort }: FindCategoryQuery,
  ): PromiseJSONResponse<Category[], PaginatedResultsMeta> {
    const allowedCols = ['title'];
    return this.categoryService
      .findWithPaginate({
        perPage,
        page,
        sort: parseSortQuery(sort, { allowedCols }),
        filter: {
          title: filter?.title,
        },
      })
      .then((res) => {
        return {
          data: res.items,
          meta: res.meta,
        };
      });
  }

  @Get('/withMangaCount')
  findAllWithCount(): PromiseJSONResponse<CategoryWithCountDto[]> {
    return this.categoryService
      .getAllCategoriesWithCount()
      .then((categories) => ({ data: categories }));
  }

  @Roles(RoleType.ADMIN)
  @Post()
  createCategory(
    @Body() dto: CreateCategoryDto,
  ): PromiseJSONResponse<Category> {
    return this.categoryService.createCategory(dto).then((category) => {
      return {
        data: category,
      };
    });
  }

  @Get(':id')
  getCategoryById(@Param('id') id: string): PromiseJSONResponse<Category> {
    return this.categoryService
      .getCategoryById(id)
      .then((res) => {
        return {
          data: res,
        };
      })
      .catch((e) => {
        if (e === ErrCategoryNotExisted) {
          throw new NotFoundException(e.message);
        }
        throw e;
      });
  }

  @Roles(RoleType.ADMIN)
  @Put(':id')
  updateCategory(@Param('id') id: string, @Body() dto: UpdateCategoryDto) {
    return this.categoryService.updateCategory(id, dto).then((category) => {
      return category;
    });
  }

  @Roles(RoleType.ADMIN)
  @Delete(':id')
  removeCategory(@Param('id') id: string): PromiseJSONResponse<Category> {
    return this.categoryService.deleteCategory(id).then((category) => {
      return {
        data: category,
      };
    });
  }
}
