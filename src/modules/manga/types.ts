import { MangaStatus } from 'src/constant';
import { SortOptions } from '../../utils/parse-sort-query';

export interface FindMangaFilterOptions {
  langCodes?: string[];
  status?: MangaStatus[];
  authors?: string[]; // list of author's object id.
  categories?: string[]; // list of categories'object id.
}

export interface FindMangaOptions {
  perPage: number;
  page: number;
  include?: string[];
  q?: string;
  sort?: SortOptions;
  filter?: FindMangaFilterOptions;
}

export interface FindLatestChapterOptions {
  limit: number;
  langCode: string;
}

export interface FindRecommendedMangaFilterOptions {
  isPublished?: boolean;
}

export interface FindRecommendedMangaOptions {
  filter?: FindRecommendedMangaFilterOptions;
}

export interface FindCategoryFilterOptions {
  title?: string;
}

export interface FindCategoryOptions {
  perPage: number;
  page: number;
  sort?: SortOptions;
  filter?: FindCategoryFilterOptions;
}
