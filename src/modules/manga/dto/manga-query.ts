import { Transform, Type } from 'class-transformer';
import {
  IsEnum,
  IsIn,
  IsMongoId,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { MangaStatus } from '../../../constant';
import { languageCodes } from '../../../assets/lang';
import { OffsetPagingQuery } from '../../../utils/paging-query';

const allowedInclude: string[] = ['categories', 'chapters', 'authors'];

export class MangaFilterQuery {
  @Transform(({ value }) => (value != null ? value.split(',') : undefined))
  @IsIn(languageCodes, { each: true })
  @IsOptional()
  translatedLanguage: string[] = [];

  @IsEnum(MangaStatus, { each: true })
  @IsOptional()
  statuses: MangaStatus[] = [];

  @IsMongoId({ each: true })
  @IsOptional()
  authors: string[] = [];

  @IsMongoId({ each: true })
  @IsOptional()
  categories: string[] = [];
}

export class MangaQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  sort?: string[];

  @IsIn(allowedInclude, { each: true })
  @IsOptional()
  include?: string[];

  @IsString()
  @IsOptional()
  q?: string;

  @ValidateNested()
  @Type(() => MangaFilterQuery)
  @IsOptional()
  filter?: MangaFilterQuery;
}
