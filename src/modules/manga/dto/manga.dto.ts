import { AgeRating, MangaStatus, MangaType } from '../../../constant';
import { Author } from '../../author/schemas/author.schema';
import { Category } from '../schemas/category.schema';
import { Chapter } from '../schemas/chapter.schema';
import { CoverImage } from '../schemas/cover-image.schema';
import { ChapterDto } from './chapter.dto';

/**
 * DTO for manga object.
 */
export class MangaDto {
  id: string;
  title: string;
  romajiTitle: string;
  synopsis?: string;
  authors?: Author[];
  categories?: Category[];
  mangaType: MangaType;
  status: MangaStatus;
  coverImage: CoverImage;
  createdAt?: Date;
  ageRating: AgeRating;
  chapters?: ChapterDto[];
}

// MangaStatusDto ...
export class MangaStatusDto {
  status: {
    name: string;
    value: string;
  };
  mangaCount: number;
}
