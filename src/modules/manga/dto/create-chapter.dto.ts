import {
  IsDefined,
  IsIn,
  IsNumberString,
  IsString,
  IsUrl,
} from 'class-validator';
import { languageCodes } from '../../../assets/lang';
import { LanguageCodeToValue } from '../schemas/title.schema';

export class CreateChapterDto {
  @IsDefined()
  titles: LanguageCodeToValue;

  @IsString()
  canonicalTitle: string;

  @IsUrl({}, { each: true })
  pages: string[];

  @IsUrl()
  thumbnailUrl: string;

  @IsNumberString()
  number: string;

  @IsIn(languageCodes)
  originalLanguage: string;

  @IsIn(languageCodes)
  translatedLanguage: string;
}
