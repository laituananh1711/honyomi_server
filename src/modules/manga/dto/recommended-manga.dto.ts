import { MangaDto } from './manga.dto';

export class RecommendedMangaDto {
  id: string;
  titleColor: string;
  description: string;
  manga: MangaDto | string;
  isPublished: boolean;
  imageUrl: string;
  createdAt: Date;
}
