import {
  IsDefined,
  IsEnum,
  IsMongoId,
  IsOptional,
  IsString,
} from 'class-validator';
import { AgeRating, MangaStatus, MangaType } from '../../../constant';
import { CoverImage } from '../schemas/cover-image.schema';
import { LanguageCodeToValue } from '../schemas/title.schema';

export class CreateMangaDto {
  @IsDefined()
  titles: LanguageCodeToValue;

  @IsString()
  romajiTitle: string;

  @IsString()
  canonicalTitle: string;

  @IsEnum(AgeRating)
  ageRating: AgeRating;

  @IsString()
  @IsOptional()
  synopsis?: string;

  @IsDefined()
  coverImage: CoverImage;

  @IsMongoId({ each: true })
  @IsOptional()
  authors: string[] = [];

  @IsMongoId({ each: true })
  @IsOptional()
  categories: string[] = [];

  @IsEnum(MangaType)
  mangaType: MangaType;

  @IsEnum(MangaStatus)
  status: MangaStatus;
}
