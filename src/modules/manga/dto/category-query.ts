import { Type } from 'class-transformer';
import { IsOptional, IsString, ValidateNested } from 'class-validator';
import { OffsetPagingQuery } from '../../../utils/paging-query';

export class FindCategoryFilterQuery {
  @IsOptional()
  @IsString()
  title?: string;
}
export class FindCategoryQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  sort: string[] = [];

  @ValidateNested()
  @Type(() => FindCategoryFilterQuery)
  @IsOptional()
  filter?: FindCategoryFilterQuery;
}
