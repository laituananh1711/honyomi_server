import { MangaDto } from './manga.dto';

/**
 * DTO for chapter, used when responding to requests.
 */
export class ChapterDto {
  id: string;
  // translated title based on user language preferences.
  title: string;
  translatedLanguage?: string;
  pages?: string[];
  thumbnailUrl: string;
  manga: MangaDto | string;
  number: string;
}
