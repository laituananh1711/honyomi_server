import { Category } from '../schemas/category.schema';

export class CategoryWithCountDto {
  category: Category;
  mangaCount: number;
}
