import { Transform } from 'class-transformer';
import {
  IsBoolean,
  IsHexColor,
  IsMongoId,
  IsOptional,
  IsString,
  IsUrl,
} from 'class-validator';

export class CreateRecommendedMangaDto {
  @IsMongoId()
  mangaId: string;

  @IsHexColor()
  titleColor: string;

  @IsString()
  @IsOptional()
  description?: string;

  @IsBoolean()
  isPublished: boolean;

  @IsUrl()
  imageUrl: string;
}

export class UpdateRecommendedMangaDto {
  @IsHexColor()
  @IsOptional()
  titleColor?: string;

  @IsString()
  @IsOptional()
  description?: string;

  @IsBoolean()
  @IsOptional()
  isPublished?: boolean;

  @IsUrl()
  @IsOptional()
  imageUrl?: string;
}

export class GetRecommendedMangaQuery {
  @IsOptional()
  @Transform(({ value }) => value === 'true' || value === '')
  isPublished?: boolean;
}
