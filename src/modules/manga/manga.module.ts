import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MangaController } from './manga.controller';
import { MangaService } from './manga.service';
import { Category, CategorySchema } from './schemas/category.schema';
import { Chapter, ChapterSchema } from './schemas/chapter.schema';
import { Manga, MangaSchema } from './schemas/manga.schema';
import {
  RecommendedManga,
  RecommendedMangaSchema,
} from './schemas/recommended-manga.schema';
import { CategoryController } from './category.controller';
import { CategoryService } from './category.service';
import { CommentModule } from '../comment/comment.module';
import { ChapterController } from './chapter.controller';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Chapter.name, schema: ChapterSchema },
      { name: Manga.name, schema: MangaSchema },
      { name: Category.name, schema: CategorySchema },
      { name: RecommendedManga.name, schema: RecommendedMangaSchema },
    ]),
    CommentModule,
  ],
  controllers: [MangaController, CategoryController, ChapterController],
  providers: [MangaService, CategoryService],
  exports: [MongooseModule, MangaService],
})
export class MangaModule {}
