import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Category, CategoryDocument } from './schemas/category.schema';
import { Model, Types } from 'mongoose';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { Manga, MangaDocument } from './schemas/manga.schema';
import { ErrCategoryNotExisted } from './errors';
import { CategoryWithCountDto } from './dto/category-with-count.dto';
import { FindCategoryOptions } from './types';
import { paginate, PaginatedResults } from '../../utils/paginate';

@Injectable()
export class CategoryService {
  constructor(
    @InjectModel(Category.name) private categoryModel: Model<CategoryDocument>,
    @InjectModel(Manga.name) private mangaModel: Model<MangaDocument>,
  ) {}

  createCategory(dto: CreateCategoryDto): Promise<Category> {
    const newCategory = new this.categoryModel(dto);
    return newCategory.save();
  }

  findWithPaginate({
    perPage,
    page,
    filter,
    sort,
  }: FindCategoryOptions): Promise<PaginatedResults<Category>> {
    let query = this.categoryModel.find({}, {});

    if (filter != null) {
      if (filter.title != null && filter.title.length > 0) {
        query = query.find({ title: new RegExp(filter.title, 'i') });
      }
    }

    if (sort != null) {
      query = query.sort(sort);
    }

    return paginate(query, { perPage, page });
  }

  getCategoryById(id: string): Promise<Category> {
    return this.categoryModel
      .findById(new Types.ObjectId(id))
      .exec()
      .then((res) => {
        if (res == null) {
          throw ErrCategoryNotExisted;
        }

        return res;
      });
  }

  getAllCategories(): Promise<Category[]> {
    return this.categoryModel.find().exec();
  }
  //TODO: persistent alternative query
  getAllCategoriesWithCount(): Promise<CategoryWithCountDto[]> {
    return this.mangaModel
      .aggregate([
        { $unwind: '$categories' },
        {
          $group: {
            _id: '$categories',
            count: { $sum: 1 },
          }, //group and count manga
        },
        {
          $lookup: {
            from: this.categoryModel.collection.name,
            localField: '_id',
            foreignField: '_id',
            as: 'category',
          }, //left join categories field
        },
        { $unwind: '$category' },
        //Normalize response
        { $addFields: { 'category.id': '$category._id' } },
        { $project: { category: 1, mangaCount: '$count' } },
        {
          $project: {
            _id: 0,
            category: { _id: 0, __v: 0 },
          },
        },
      ])
      .exec();
  }

  updateCategory(id: string, dto: UpdateCategoryDto): Promise<Category> {
    return this.categoryModel
      .findByIdAndUpdate(id, dto, { new: true })
      .then((category) => {
        if (!category) {
          throw ErrCategoryNotExisted;
        }
        return category;
      });
  }

  async deleteCategory(id: string): Promise<Category> {
    const category = await this.categoryModel.findById(id).exec();

    if (!category) {
      throw ErrCategoryNotExisted;
    }

    await category.delete();
    await this.mangaModel.updateMany(
      {},
      {
        $pull: {
          categories: new Types.ObjectId(id),
        },
      },
    );
    return category;
  }
}
