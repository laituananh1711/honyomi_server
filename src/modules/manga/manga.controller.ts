import {
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { I18nLang } from 'nestjs-i18n';
import { RoleType } from '../../constant';
import { PaginatedResultsMeta } from '../../utils/paginate';
import { parseSortQuery } from '../../utils/parse-sort-query';
import { PromiseJSONResponse } from '../../utils/types';
import { Roles } from '../auth/decorators/role.decorator';
import { ChapterDto } from './dto/chapter.dto';
import { CreateChapterDto } from './dto/create-chapter.dto';
import { CreateMangaDto } from './dto/create-manga.dto';
import { MangaQuery } from './dto/manga-query';
import { MangaDto, MangaStatusDto } from './dto/manga.dto';
import {
  CreateRecommendedMangaDto,
  GetRecommendedMangaQuery,
  UpdateRecommendedMangaDto,
} from './dto/recommend-manga.dto';
import { RecommendedMangaDto } from './dto/recommended-manga.dto';
import { ErrMangaNotFound } from './errors';
import { MangaService } from './manga.service';
import { Chapter, toChapterDto } from './schemas/chapter.schema';
import { Manga, toMangaDto } from './schemas/manga.schema';
import {
  RecommendedManga,
  toRecommendedMangaDto,
} from './schemas/recommended-manga.schema';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { UserInfo } from '../auth/decorators/user.decorator';
import { UserPayload } from '../auth/interfaces/user-payload';
import { CreateCommentDto } from './dto/create-comment.dto';
import { CommentService } from '../comment/comment.service';
import { Comment } from '../comment/schemas/comment.schema';

@Controller('manga')
export class MangaController {
  constructor(
    private mangaService: MangaService,
    private commentService: CommentService,
  ) {}

  @Get()
  findAll(
    @Query() { page, perPage, include, sort, q, filter }: MangaQuery,
    @I18nLang() lang: string,
  ): PromiseJSONResponse<MangaDto[], PaginatedResultsMeta> {
    const allowedSortCols = ['romajiTitle', 'createdAt', 'canonicalTitle'];

    return this.mangaService
      .findAll({
        page,
        perPage,
        include,
        sort: parseSortQuery(sort ?? [], { allowedCols: allowedSortCols }),
        q,
        filter: {
          langCodes: filter?.translatedLanguage,
          status: filter?.statuses,
          authors: filter?.authors,
          categories: filter?.categories,
        },
      })
      .then((resp) => ({
        data: resp.items.map((item) => toMangaDto(item, lang)),
        meta: resp.meta,
      }));
  }

  @Roles(RoleType.ADMIN)
  @Post()
  create(@Body() dto: CreateMangaDto): PromiseJSONResponse<Manga> {
    return this.mangaService.create(dto).then((resp) => ({ data: resp }));
  }

  @Get('chapters/latest')
  findLatestChapters(
    @Query('lang', new DefaultValuePipe('en')) langCode: string,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
  ): PromiseJSONResponse<ChapterDto[]> {
    return this.mangaService
      .findLatestChapters({ limit, langCode })
      .then((res) => ({
        data: res.map((item) => toChapterDto(item, langCode)),
      }));
  }

  // NOTE: limit should have a 'limit', get it?
  @Get('most-popular')
  findMostPopularManga(
    @I18nLang() lang: string,
    @Query('limit', new DefaultValuePipe(6), ParseIntPipe) limit: number,
  ): PromiseJSONResponse<{ manga: MangaDto; viewCount: number }[]> {
    return this.mangaService
      .getMostPopularManga(limit)
      .then((res) => {
        return res.map((item) => ({
          manga: toMangaDto(item.manga, lang),
          viewCount: item.viewCount,
        }));
      })
      .then((res) => ({ data: res }));
  }

  @Get('recommended')
  findRecommendedManga(
    @Query() filterQuery: GetRecommendedMangaQuery,
    @I18nLang() lang: string,
  ): PromiseJSONResponse<RecommendedMangaDto[]> {
    return this.mangaService
      .findAllRecommended({ filter: filterQuery })
      .then((resp) => ({
        data: resp.map((item) => toRecommendedMangaDto(item, lang)),
      }));
  }

  @Roles(RoleType.ADMIN)
  @Post('recommended')
  createRecommendedManga(
    @Body() dto: CreateRecommendedMangaDto,
  ): PromiseJSONResponse<RecommendedManga> {
    return this.mangaService
      .createRecommendedManga(dto)
      .then((res) => ({ data: res }));
  }

  @Roles(RoleType.ADMIN)
  @Put('recommended/:id')
  updateRecommendedManga(
    @Param('id') id: string,
    @Body() dto: UpdateRecommendedMangaDto,
  ): PromiseJSONResponse<RecommendedManga> {
    return this.mangaService.updateRecommendedManga(id, dto).then((res) => {
      if (res == null) {
        throw ErrMangaNotFound;
      }
      return { data: res };
    });
  }

  @Get('recommended/:id')
  getRecommendedById(
    @Param('id') id: string,
  ): PromiseJSONResponse<RecommendedManga> {
    return this.mangaService.getRecommendedById(id).then((res) => {
      if (res == null) {
        throw ErrMangaNotFound;
      }
      return { data: res };
    });
  }

  @Roles(RoleType.ADMIN)
  @Post(':id/chapters')
  createChapter(
    @Param('id') mangaId: string,
    @Body() dto: CreateChapterDto,
  ): PromiseJSONResponse<Chapter> {
    return this.mangaService.createChapter(mangaId, dto).then((resp) => ({
      data: resp,
    }));
  }

  @Roles(RoleType.ADMIN)
  @Delete('chapters/:chapterId')
  deleteChapter(
    @Param('chapterId') id: string,
  ): PromiseJSONResponse<Chapter | null> {
    return this.mangaService.deleteChapter(id).then((res) => ({ data: res }));
  }

  @Get('status')
  getMangaStatus(): PromiseJSONResponse<MangaStatusDto[]> {
    return this.mangaService.getMangaStatusWithCount().then((res) => ({
      data: res,
    }));
  }

  @Get(':id')
  findOne(
    @Param('id') id: string,
    @I18nLang() lang: string,
  ): PromiseJSONResponse<MangaDto> {
    return this.mangaService.findOne(id).then((resp) => {
      if (resp == null) {
        throw ErrMangaNotFound;
      }
      return {
        data: toMangaDto(resp, lang),
      };
    });
  }
}
