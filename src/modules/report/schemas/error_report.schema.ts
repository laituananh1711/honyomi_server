import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as SchemaMongoose } from 'mongoose';

export enum ErrorReportType {
  IMAGE_LOAD_FAILED = 'IMAGE_LOAD_FAILED',
  OTHER = 'OTHER',
}

export enum ErrorReportStatus {
  PENDING = 'PENDING',
  RESOLVED = 'RESOLVED',
  CLOSED = 'CLOSED',
}

export type ErrorReportDocument = ErrorReport & Document;

@Schema()
export class ErrorReport {
  @Prop({
    enum: Object.values(ErrorReportType),
    required: true,
  })
  type: ErrorReportType;

  @Prop({
    enum: Object.values(ErrorReportStatus),
    default: ErrorReportStatus.PENDING,
  })
  status: ErrorReportStatus;

  @Prop({ required: true })
  description: string;

  @Prop({ required: false })
  email: string;

  @Prop({ required: false })
  url: string;

  @Prop({ required: false })
  screenshotUrl: string;

  @Prop({ type: SchemaMongoose.Types.Mixed })
  otherInfo: any;

  @Prop({ default: Date.now })
  createdAt: Date;

  @Prop({ default: Date.now })
  updatedAt: Date;
}

export const ErrorReportSchema = SchemaFactory.createForClass(ErrorReport);
