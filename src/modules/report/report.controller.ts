import { Body, Controller, Get, Post } from '@nestjs/common';
import { PromiseJSONResponse } from '../../utils/types';
import { CreateReportDto } from './dto/create-report.dto';
import { ReportService } from './report.service';
import { ErrorReport } from './schemas/error_report.schema';

@Controller('reports')
export class ReportController {
  constructor(private service: ReportService) {}

  @Post()
  create(@Body() dto: CreateReportDto): PromiseJSONResponse<ErrorReport> {
    return this.service.create(dto).then((item) => ({ data: item }));
  }

  @Get()
  findAll(): PromiseJSONResponse<ErrorReport[]> {
    return this.service.findAll().then((items) => ({ data: items }));
  }
}
