import { IsEmail, IsEnum, IsOptional, IsString, IsUrl } from 'class-validator';
import {
  ErrorReportStatus,
  ErrorReportType,
} from '../schemas/error_report.schema';

export class CreateReportDto {
  @IsEnum(ErrorReportType)
  type: ErrorReportType;

  @IsEnum(ErrorReportStatus)
  status: ErrorReportStatus;

  @IsString()
  description: string;

  @IsEmail()
  @IsOptional()
  email?: string;

  @IsUrl()
  @IsOptional()
  url?: string;

  @IsUrl()
  @IsOptional()
  screenshotUrl?: string;

  @IsOptional()
  otherInfo: unknown;
}
