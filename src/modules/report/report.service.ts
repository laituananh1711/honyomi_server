import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateReportDto } from './dto/create-report.dto';
import {
  ErrorReport,
  ErrorReportDocument,
} from './schemas/error_report.schema';

@Injectable()
export class ReportService {
  constructor(
    @InjectModel(ErrorReport.name)
    private errorReportModel: Model<ErrorReportDocument>,
  ) {}

  create(dto: CreateReportDto): Promise<ErrorReport> {
    const newReport = new this.errorReportModel(dto);
    return newReport.save();
  }

  findAll(): Promise<ErrorReport[]> {
    return this.errorReportModel.find().exec();
  }
}
