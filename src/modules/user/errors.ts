export const ErrUserNotFound = new Error('errors.UserNotFound');
ErrUserNotFound.name = 'ErrUserNotFound';

export const ErrEmailExisted = new Error('errors.emailExisted');
ErrEmailExisted.name = 'ErrEmailExisted';

export const ErrUsernameExisted = new Error('errors.usernameExisted');
ErrUsernameExisted.name = 'ErrUsernameExisted';

const ErrWrongPassword = new Error('errors.wrongPassword');
ErrWrongPassword.name = 'ErrWrongPassword';

export { ErrWrongPassword };
