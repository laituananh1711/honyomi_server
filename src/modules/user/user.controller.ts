import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDTO } from './dto/create-user.dto';
import { Roles } from '../auth/decorators/role.decorator';
import { RoleType } from '../../constant';
import { User } from './schemas/user.schema';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserInterceptor } from './interceptor/user.interceptor';
import { PaginatedResultsMeta } from '../../utils/paginate';
import { PromiseJSONResponse } from '../../utils/types';
import { UserDto } from './dto/user.dto';
import { UserQuery } from './dto/user-query';
import { parseSortQuery } from '../../utils/parse-sort-query';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  @Roles(RoleType.ADMIN)
  getAllUser(
    @Query() { page, perPage, filter, sort }: UserQuery,
  ): PromiseJSONResponse<UserDto[], PaginatedResultsMeta> {
    const allowedSortCols = ['username', 'email', 'createdAt'];
    return this.userService
      .getAllUsers({
        page,
        perPage,
        filter: {
          username: filter?.username,
          email: filter?.email,
        },
        sort: parseSortQuery(sort ?? [], { allowedCols: allowedSortCols }),
      })
      .then((resp) => {
        return {
          data: resp.items.map((user) => new UserDto(user)),
          meta: resp.meta,
        };
      });
  }

  @Get(':id')
  @Roles(RoleType.ADMIN)
  @UseInterceptors(UserInterceptor)
  getUser(@Param('id') id: string) {
    return this.userService.findProfileById(id).catch((e) => {
      throw new BadRequestException(e);
    });
  }

  @Post()
  @UseInterceptors(UserInterceptor)
  @Roles(RoleType.ADMIN)
  @HttpCode(HttpStatus.ACCEPTED)
  addUser(@Body() dto: CreateUserDTO): Promise<User> {
    return this.userService.createUser(dto).catch((e) => {
      throw new BadRequestException(e);
    });
  }

  @Put(':id')
  @Roles(RoleType.ADMIN)
  @UseInterceptors(UserInterceptor)
  updateUser(
    @Param('id') id: string,
    @Body() dto: UpdateUserDto,
  ): Promise<User | null> {
    return this.userService.updateProfileById(id, dto).catch((e) => {
      throw new BadRequestException(e);
    });
  }
}
