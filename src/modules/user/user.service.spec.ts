import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { EventEmitter2 } from 'eventemitter2';
import * as mongoose from 'mongoose';
import { Favorite, FavoriteSchema } from './schemas/favorite.schema';
import { History, HistorySchema } from './schemas/history.schema';
import { User, UserSchema } from './schemas/user.schema';
import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getModelToken(User.name),
          useValue: mongoose.model(User.name, UserSchema),
        },
        {
          provide: getModelToken(Favorite.name),
          useValue: mongoose.model(Favorite.name, FavoriteSchema),
        },
        {
          provide: getModelToken(History.name),
          useValue: mongoose.model(History.name, HistorySchema),
        },
        {
          provide: EventEmitter2,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
