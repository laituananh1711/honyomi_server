import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { HASHING_ROUND, RoleType } from '../../../constant';
import { compareSync, hashSync } from 'bcrypt';

export type UserDocument = User & Document;

export enum UITheme {
  DARK = 'dark',
  LIGHT = 'light',
}

@Schema({ _id: false })
export class Setting extends Document {
  @Prop({ required: true, default: 'en' })
  language: string;

  @Prop({ required: true, default: UITheme.DARK, enum: Object.values(UITheme) })
  uiTheme: UITheme;

  @Prop({ required: true, type: [{ type: String }], default: ['en'] })
  mangaLanguages: string[];

  @Prop({ required: true, default: false })
  receiveMailNewChapter: boolean;

  @Prop({ required: true, default: true })
  receiveNotificationNewChapter: boolean;
}
export const SettingSchema = SchemaFactory.createForClass(Setting);

@Schema({
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
  },
})
export class User {
  @Prop({ required: true })
  username: string;

  @Prop({ required: true })
  password: string;

  @Prop({ required: true })
  email: string;

  @Prop({
    enum: Object.values(RoleType),
    default: RoleType.USER,
  })
  role: RoleType;

  @Prop({
    required: true,
    type: SettingSchema,
    default: () => ({}),
  })
  setting: Setting;

  @Prop({
    required: true,
    type: String,
    default: 'https://gravatar.com/avatar/?d=mp',
  })
  avatarUrl: string;

  validatePassword!: (password: string) => boolean;
  id: string;

  createdAt?: Date;
  updatedAt?: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.pre<UserDocument>('save', function save(next) {
  if (!this.isModified('password')) {
    return next();
  }

  try {
    this.password = hashSync(this.password as string, HASHING_ROUND);
    return next();
  } catch (e: any) {
    return next(e);
  }
});

UserSchema.methods.validatePassword = function (password: string) {
  return compareSync(password, this.get('password'));
};

UserSchema.virtual('id').get(function (this: UserDocument) {
  return this._id;
});
