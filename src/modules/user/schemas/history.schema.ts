import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Schema as MongooseSchema, Types } from 'mongoose';
import { Chapter } from '../../manga/schemas/chapter.schema';
import { User } from './user.schema';

export type HistoryDocument = History & Document;

@Schema({ timestamps: true })
export class History {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: User.name })
  user: User | Types.ObjectId;

  @Prop({ type: MongooseSchema.Types.ObjectId, ref: Chapter.name })
  chapter: Chapter;

  @Prop({ default: Date.now })
  createdAt: Date;
}

export const HistorySchema = SchemaFactory.createForClass(History);
