import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema, Types } from 'mongoose';
import { User } from './user.schema';
import { Manga } from '../../manga/schemas/manga.schema';

export type FavoriteDocument = Favorite & Document;

@Schema({ timestamps: true })
export class Favorite {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: User.name, required: true })
  user: User | Types.ObjectId;

  @Prop({
    type: [{ type: MongooseSchema.Types.ObjectId, ref: Manga.name }],
    default: [],
  })
  mangas: Manga[] | Types.ObjectId[];
}

export const FavoriteSchema = SchemaFactory.createForClass(Favorite);
