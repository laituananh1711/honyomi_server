import { Body, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { EventEmitter2 } from 'eventemitter2';
import { Model, PopulateOptions, Types } from 'mongoose';
import { events } from '../../constant';
import { paginate, PaginatedResults } from '../../utils/paginate';
import { toChapterDto } from '../manga/schemas/chapter.schema';
import { Manga } from '../manga/schemas/manga.schema';
import { ChangePasswordDto } from '../me/dto/change-password.dto';
import { CreateUserDTO } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import {
  ErrEmailExisted,
  ErrUsernameExisted,
  ErrUserNotFound,
  ErrWrongPassword,
} from './errors';
import { UserCreatedEvent } from './events';
import { Favorite, FavoriteDocument } from './schemas/favorite.schema';
import { History, HistoryDocument } from './schemas/history.schema';
import { User, UserDocument } from './schemas/user.schema';
import { FindUserOptions } from './types';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>,
    @InjectModel(Favorite.name)
    private readonly favoriteModel: Model<FavoriteDocument>,
    @InjectModel(History.name)
    private readonly historyModel: Model<HistoryDocument>,
    private eventEmitter: EventEmitter2,
  ) {}

  getAllUsers({
    page,
    perPage,
    filter,
    sort,
  }: FindUserOptions): Promise<PaginatedResults<User>> {
    let query = this.userModel.find(
      {},
      {
        createdAt: 1,
        username: 1,
        email: 1,
        role: 1,
        setting: 1,
        avatarUrl: 1,
        updatedAt: 1,
      },
    );
    if (filter) {
      if (filter.username) {
        query = query.find({
          username: new RegExp(filter.username, 'i'),
        });
      }

      if (filter.email) {
        query = query.find({
          email: new RegExp(filter.email, 'i'),
        });
      }

      if (filter.role) {
        query = query.find({
          role: filter.role,
        });
      }
    }

    if (sort) {
      query = query.sort(sort);
    }

    return paginate(query, { page, perPage });
  }

  async createUser(dto: CreateUserDTO): Promise<User> {
    const userExisted = await this.userModel
      .findOne({
        $or: [{ username: dto.username }, { email: dto.email }],
      })
      .exec();

    if (userExisted) {
      if (userExisted.username == dto.username) {
        throw ErrUsernameExisted;
      } else {
        throw ErrEmailExisted;
      }
    }
    return new this.userModel(dto).save().then((res) => {
      this.eventEmitter.emit(
        events.USER_CREATED,
        new UserCreatedEvent(res.username, res.email),
      );
      return res;
    });
  }

  findByUsername(username: string): Promise<User | null> {
    return this.userModel.findOne({ username: username }).exec();
  }

  findProfileById(id: string | Types.ObjectId): Promise<User | null> {
    return this.userModel.findById(id, { password: 0 }).exec();
  }

  findUserByEmail(email: string): Promise<User | null> {
    return this.userModel.findOne({ email: email }).exec();
  }

  async updatePasswordById(
    id: string,
    dto: ChangePasswordDto,
  ): Promise<User | null> {
    const user = await this.userModel.findById(id).exec();
    if (user == null) {
      throw ErrUserNotFound;
    }

    if (!user.validatePassword(dto.currentPassword)) {
      throw ErrWrongPassword;
    }

    user.password = dto.newPassword;
    return new this.userModel(user).save();
  }

  async updateProfileById(
    id: string | Types.ObjectId,
    @Body() dto: UpdateUserDto,
  ): Promise<User | null> {
    const user = (await this.findProfileById(id)) as UserDocument;

    if (!user) {
      throw ErrUserNotFound;
    }

    if (dto.email) {
      const userOwnEmail = await this.userModel
        .findOne({ email: dto.email })
        .exec();

      if (userOwnEmail && userOwnEmail.id !== id) {
        throw ErrEmailExisted;
      }
    }

    Object.assign(user, dto);
    await user.save();
    return user;
  }

  private favoritePopulateOptions: PopulateOptions = {
    path: 'mangas',
    populate: [{ path: 'authors' }, { path: 'categories' }],
  };

  findFavoriteByUserId(userId: string | Types.ObjectId): Promise<Manga[]> {
    return this.favoriteModel
      .findOneAndUpdate(
        { user: userId },
        {},
        {
          populate: this.favoritePopulateOptions,
          upsert: true,
          new: true,
          setDefaultsOnInsert: true,
        },
      )
      .exec()
      .then((record) => {
        return record.mangas as Manga[];
      });
  }

  addNewFavorite(
    userId: string | Types.ObjectId,
    mangaId: string | Types.ObjectId,
  ): Promise<Manga[]> {
    return this.favoriteModel
      .findOneAndUpdate(
        {
          user: userId,
        },
        {
          $addToSet: {
            mangas: mangaId,
          },
        },
        {
          populate: this.favoritePopulateOptions,
          upsert: true,
          new: true,
          setDefaultsOnInsert: true,
        },
      )
      .exec()
      .then((record) => {
        return record.mangas as Manga[];
      });
  }

  removeFavorite(
    userId: string | Types.ObjectId,
    mangaId: string | Types.ObjectId,
  ): Promise<Manga[]> {
    return this.favoriteModel
      .findOneAndUpdate(
        {
          user: userId,
        },
        {
          $pull: {
            mangas: mangaId,
          },
        },
        {
          populate: this.favoritePopulateOptions,
          upsert: true,
          new: true,
          setDefaultsOnInsert: true,
        },
      )
      .exec()
      .then((record) => {
        return record.mangas as Manga[];
      });
  }

  async isFavorite(userId: string, mangaId: string): Promise<boolean> {
    return this.favoriteModel
      .findOne({
        user: new Types.ObjectId(userId),
        mangas: new Types.ObjectId(mangaId),
      })
      .then((res) => res != null);
  }

  /**
   * Record which chapters an user has seen.
   * @param userId id of user
   * @param chapterId id of chapter
   */
  async addToHistory(userId: string, chapterId: string): Promise<void> {
    const newEntry = new this.historyModel({
      user: userId,
      chapter: chapterId,
    });

    await newEntry.save();
  }

  /**
   * Return list of read chapters.
   * @param userId
   * @param langCode
   * @returns
   */
  async getHistory(userId: string): Promise<Manga[]> {
    const readMangas: Manga[] = await this.historyModel
      .aggregate([
        { $match: { user: new Types.ObjectId(userId) } },
        {
          $lookup: {
            from: 'chapters',
            localField: 'chapter',
            foreignField: '_id',
            as: 'chapter',
          },
        },
        { $unwind: '$chapter' },
        {
          $addFields: {
            'chapter.id': '$chapter._id',
          },
        },
        {
          $project: {
            chapter: { _id: 0, __v: 0, pages: 0 },
          },
        },
        {
          $group: {
            _id: '$chapter.manga',
            chapters: { $addToSet: '$chapter' },
          },
        },
        {
          $lookup: {
            from: 'mangas',
            localField: '_id',
            foreignField: '_id',
            as: 'manga',
          },
        },
        { $unwind: '$manga' },
        { $addFields: { 'manga.id': '$_id', 'manga.chapters': '$chapters' } },
        { $replaceRoot: { newRoot: '$manga' } },
        { $project: { _id: 0, __v: 0 } },
      ])
      .exec();

    return readMangas;
  }

  getHistoryAsChapters(userId: string, langCode: string) {
    return this.historyModel
      .find({
        user: userId,
      })
      .populate({
        path: 'chapter',
        select: 'titles canonicalTitle thumbnailUrl number',
        populate: {
          path: 'manga',
          select: 'titles canonicalTitle romajiTitle coverImage',
        },
      })
      .sort({
        createdAt: -1,
      })
      .exec()
      .then((items) =>
        items.map((item) => ({
          userId: item.user,
          createdAt: item.createdAt,
          chapter: toChapterDto(item.chapter, langCode),
        })),
      );
  }

  getLikedUsersByMangaId(mangaId: string): Promise<Types.ObjectId[]> {
    return this.favoriteModel
      .find(
        {
          mangas: new Types.ObjectId(mangaId),
        },
        {
          user: 1,
        },
      )
      .exec()
      .then((res) => res.map((item) => item.user as Types.ObjectId));
  }

  // getNewChapterMailedUsers returns a list of users who would like to
  // receive emails on new chapter release.
  getNewChapterMailedUsers(userIds: Types.ObjectId[]): Promise<User[]> {
    return this.userModel
      .find({
        _id: {
          $in: userIds,
        },
        'setting.receiveMailNewChapter': true,
      } as any)
      .exec();
  }

  // Get users who would like to be notified when a new chapter for
  // a manga is released.
  async getNotifiedUserByMangaId(mangaId: string): Promise<User[]> {
    const userIds = await this.getLikedUsersByMangaId(mangaId);

    return this.userModel
      .find({
        _id: {
          $in: userIds,
        },
        'setting.receiveNotificationNewChapter': true,
      })
      .exec();
  }
}
