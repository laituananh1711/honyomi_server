import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { JSONResponse } from '../../../utils/types';
import { User } from '../schemas/user.schema';
import { UserDto } from '../dto/user.dto';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

//TODO: separate interceptor into two interceptors: transform to resource and wrap to response
@Injectable()
export class UserInterceptor
  implements NestInterceptor<User | User[], JSONResponse>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler<User | User[]>,
  ): Observable<JSONResponse> | Promise<Observable<JSONResponse>> {
    return next.handle().pipe(
      map((resource) => {
        if (Array.isArray(resource)) {
          return {
            data: resource.map((user) => {
              return new UserDto(user);
            }),
          };
        } else {
          return { data: new UserDto(resource) };
        }
      }),
    );
  }
}
