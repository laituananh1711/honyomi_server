import { PASSWORD_LENGTH, RoleType, USERNAME_LENGTH } from '../../../constant';
import {
  IsEmail,
  IsIn,
  IsOptional,
  IsString,
  IsUrl,
  Length,
} from 'class-validator';

export class CreateUserDTO {
  @IsString()
  @Length(USERNAME_LENGTH.min, USERNAME_LENGTH.max)
  username: string;

  @IsString()
  @Length(PASSWORD_LENGTH.min, PASSWORD_LENGTH.max)
  password: string;

  @IsEmail()
  email: string;

  @IsString()
  @IsIn([RoleType.USER, RoleType.ADMIN])
  @IsOptional()
  role?: RoleType;

  @IsUrl()
  @IsOptional()
  avatarUrl?: string;
}
