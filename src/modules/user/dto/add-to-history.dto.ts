import { IsString } from 'class-validator';

export class AddToHistoryDto {
  @IsString()
  chapterId: string;
}
