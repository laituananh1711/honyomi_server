import { OffsetPagingQuery } from '../../../utils/paging-query';
import { IsOptional, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

export class UserFilterQuery {
  @IsOptional()
  username?: string;

  @IsOptional()
  email?: string;
}

export class UserQuery extends OffsetPagingQuery {
  @IsOptional()
  @ValidateNested()
  @Type(() => UserFilterQuery)
  filter?: UserFilterQuery;

  @IsString({ each: true })
  @IsOptional()
  sort?: string[];
}
