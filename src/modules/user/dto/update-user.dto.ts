import {
  IsEmail,
  IsEnum,
  IsIn,
  IsOptional,
  IsString,
  IsUrl,
  Length,
  ValidateNested,
} from 'class-validator';
import { PASSWORD_LENGTH } from '../../../constant';
import { Type } from 'class-transformer';
import { languageCodes } from '../../../assets/lang';
import { UITheme } from '../schemas/user.schema';

// This type's fields is not optional
// because it's nested in UpdateUserDto
class SettingDto {
  @IsIn(languageCodes)
  language: string;

  @IsIn(languageCodes, { each: true })
  mangaLanguages: string[];

  @IsEnum(UITheme)
  uiTheme: UITheme;
}

export class UpdateUserDto {
  @ValidateNested()
  @Type(() => SettingDto)
  @IsOptional()
  setting?: SettingDto;

  @IsEmail()
  @IsOptional()
  email?: string;

  @IsUrl()
  @IsOptional()
  avatarUrl?: string;
}
