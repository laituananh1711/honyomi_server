import { RoleType } from '../../../constant';
import { User } from '../schemas/user.schema';

export class UserDto {
  constructor(user: User) {
    this.id = user.id;
    this.username = user.username;
    this.email = user.email;
    this.role = user.role;
    this.avatarUrl = user.avatarUrl;
    this.createdAt = user.createdAt;
    this.updatedAt = user.updatedAt;
  }

  id: string;
  username: string;
  email: string;
  role: RoleType;
  avatarUrl: string;
  createdAt?: Date;
  updatedAt?: Date;
}
