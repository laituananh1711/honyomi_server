import { SortOptions } from '../../utils/parse-sort-query';
import { RoleType } from '../../constant';

export interface FindUserOptions {
  perPage: number;
  page: number;
  filter?: UserFilterOptions;
  sort?: SortOptions;
}

export interface UserFilterOptions {
  username?: string;
  email?: string;
  role?: RoleType;
}
