export enum RoleType {
  ADMIN = 'admin',
  USER = 'user',
}

export enum NotificationStatusType {
  SENT = 'sent',
  RECEIVED = 'received',
  SEEN = 'seen',
}

export enum MangaType {
  DOUJIN = 'doujin',
  MANGA = 'manga',
  MANHUA = 'manhua',
  MANHWA = 'manhwa',
  NOVEL = 'novel',
  OEL = 'oel',
  ONESHOT = 'oneshot',
}

export enum AgeRating {
  G = 'G',
  PG = 'PG',
  R = 'R',
  R18 = 'R18',
}

export enum MangaStatus {
  ONGOING = 'ongoing',
  FINISHED = 'finished',
  DROPPED = 'dropped',
}

export const USERNAME_LENGTH = {
  min: 4,
  max: 24,
};

export const PASSWORD_LENGTH = {
  min: 8,
  max: 32,
};

export const HASHING_ROUND = 10;

export const REFRESH_TOKEN_TIME = '10d';

export const ACCESS_TOKEN_TIME = '30m';

export const FALLBACK_LANG_CODE = 'en';

export const events = {
  USER_CREATED: 'user.created',
  USER_LOGGED_IN: 'user.logged_in',
  CHAPTER_CREATED: 'chapter.new',
};
