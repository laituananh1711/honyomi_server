import * as faker from 'faker';
import { Types } from 'mongoose';
import { AgeRating, MangaStatus, MangaType, RoleType } from '../../constant';
import { Author } from '../../modules/author/schemas/author.schema';
import { Chapter } from '../../modules/manga/schemas/chapter.schema';
import { Manga } from '../../modules/manga/schemas/manga.schema';
import { LanguageCodeToValue } from '../../modules/manga/schemas/title.schema';
import { Comment } from '../../modules/comment/schemas/comment.schema';
import { random, sample } from '../utils';
import { User } from '../../modules/user/schemas/user.schema';
import { CreateUserDTO } from '../../modules/user/dto/create-user.dto';
import { Category } from '../../modules/manga/schemas/category.schema';
import { RecommendedManga } from '../../modules/manga/schemas/recommended-manga.schema';

export class ModelFactories {
  generateUser(numberOfUser: number): CreateUserDTO[] {
    const users: CreateUserDTO[] = [];
    if (numberOfUser && numberOfUser > 0) {
      for (let i = 0; i < numberOfUser; i++) {
        const user: CreateUserDTO = {
          username: faker.internet.userName(),
          password:
            '$2b$10$hLTrfSsptmKsQVCN1qE4dubsG6SSEr.s34F1P930DgWDdKwmNmzE6', //password in bcrypt
          email: faker.internet.email(),
          role: RoleType.USER,
        };
        users.push(user);
      }
    }
    return users;
  }

  generateAuthor(numberOfAuthor: number): Author[] {
    const authors: Author[] = [];
    if (numberOfAuthor > 0) {
      for (let i = 0; i < numberOfAuthor; i++) {
        const author: Author = {
          name: faker.name.findName(),
          description: faker.lorem.paragraphs(),
        };
        authors.push(author);
      }
    }
    return authors;
  }

  generateManga(numberOfManga: number): Manga[] {
    const mangas: Manga[] = [];
    for (let i = 0; i < numberOfManga; i++) {
      const title = faker.name.findName();
      const titles: LanguageCodeToValue = new Map<string, string>();
      titles.set('en', title);

      const manga: Manga = {
        titles: titles,
        romajiTitle: title,
        authors: [],
        canonicalTitle: title,
        ageRating: AgeRating.G,
        mangaType: MangaType.MANGA,
        status: MangaStatus.ONGOING,
        categories: [],
        synopsis: faker.lorem.paragraph(),
        createdAt: new Date(),
        coverImage: {
          original: {
            url: faker.image.image(),
            widthInPx: 100,
            heightInPx: 100,
          },
        },
      };
      mangas.push(manga);
    }
    return mangas;
  }

  generateChapter(numberOfChapter: number, mangaId: Types.ObjectId): Chapter[] {
    const minNumPage = 5;
    const maxNumPage = 15;

    const generatePages = (numPages: number) => {
      const pages = [];

      for (let i = 0; i < numPages; i++) {
        pages.push(faker.image.image());
      }

      return pages;
    };

    const chapters: Chapter[] = [];
    for (let i = 0; i < numberOfChapter; i++) {
      const titles = new Map();
      const title = faker.name.findName();
      titles.set('en', title);
      const chapter: Chapter = {
        titles: titles,
        translatedLanguage: 'en',
        canonicalTitle: faker.name.findName(),
        number: i.toString(),
        pages: generatePages(random(minNumPage, maxNumPage)),
        thumbnailUrl: faker.internet.avatar(),
        viewCount: random(500, 10000),
        manga: mangaId,
      };
      chapters.push(chapter);
    }
    return chapters;
  }

  generateCategories(): Category[] {
    return [
      {
        title: 'Shonen',
        description: 'Manga targeted at tween and teen boys.',
        nsfw: false,
      },
      {
        title: 'Shojo',
        description: 'Manga targeted at tween and teen girls.',
        nsfw: false,
      },
      {
        title: 'Kodomomuke',
        description: 'Manga targeted at young children.',
        nsfw: false,
      },
      {
        title: 'Seinen',
        description: 'Manga targeted at adult men (18+).',
        nsfw: true,
      },
      {
        title: 'Josei',
        description: 'Manga targeted at adult women (18+).',
        nsfw: true,
      },
    ];
  }

  generateRecommend(mangas: Manga[]): RecommendedManga[] {
    const recommends = [];
    const imageUrls = [
      'https://firebasestorage.googleapis.com/v0/b/honyomi-45907.appspot.com/o/recommended%2Famagami.png?alt=media&token=f38cfdef-d765-4239-b754-78cc15d381b2',
      'https://firebasestorage.googleapis.com/v0/b/honyomi-45907.appspot.com/o/recommended%2Fgksfd.png?alt=media&token=e078a783-2a2c-4250-9de0-905c63e8bab2',
      'https://firebasestorage.googleapis.com/v0/b/honyomi-45907.appspot.com/o/recommended%2Fkomi.png?alt=media&token=29215b87-983f-481d-be23-22950b824ffb',
      'https://firebasestorage.googleapis.com/v0/b/honyomi-45907.appspot.com/o/recommended%2Fmxm.png?alt=media&token=b8a51b22-ee02-47f2-9964-b76e8bf54400',
      'https://firebasestorage.googleapis.com/v0/b/honyomi-45907.appspot.com/o/recommended%2Fshaved.png?alt=media&token=77fa1d58-119c-49fc-bc8d-0dcdcf7938ca',
    ];

    for (let i = 0; i < imageUrls.length; i++) {
      const newRecommend: RecommendedManga = {
        titleColor: faker.internet.color(),
        description: faker.lorem.paragraph(),
        manga: mangas[i],
        isPublished: true,
        imageUrl: imageUrls[i],
        createdAt: new Date(),
      };

      recommends.push(newRecommend);
    }

    return recommends;
  }

  generateComments(
    numberOfComment: number,
    users: User[],
    chapter: Chapter,
  ): Comment[] {
    const comments: Comment[] = [];
    for (let i = 0; i < numberOfComment; i++) {
      const comment: Comment = {
        user: sample(users),
        comment: faker.lorem.sentence(),
        chapter: chapter,
        replies: [],
        repliesCount: 0,
      };
      comments.push(comment);
    }

    return comments;
  }

  generateRepliesComment(
    numberOfComment: number,
    userIds: Types.ObjectId[],
  ): Comment[] {
    const repliesComments: Comment[] = [];
    for (let i = 0; i < numberOfComment; i++) {
      const comment: Comment = {
        user: sample(userIds),
        comment: faker.lorem.paragraphs(),
        replies: [],
        repliesCount: 0,
      };

      repliesComments.push(comment);
    }

    return repliesComments;
  }
}
