import { Logger } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Command, Console } from 'nestjs-console';
import { highlight } from '../utils';

@Console({
  command: 'drop',
  description: highlight('Drop collections'),
})
export class DatabaseService {
  @Command({
    command: 'all',
    description: highlight('Drop all collection'),
  })
  drop(): Promise<void> {
    const collectionDrop: Promise<boolean>[] = [];
    mongoose.connections.forEach((connection) => {
      const collections = Object.values(connection.collections);
      if (collections.length) {
        collections.map((collection) => {
          collectionDrop.push(
            collection
              .drop()
              .then((isDropped) => {
                if (isDropped) {
                  Logger.log(`Drop ${collection.collectionName} complete`);
                } else {
                  Logger.log(`Can not ${collection.collectionName}`);
                }

                return isDropped;
              })
              .catch((e) => {
                Logger.error(
                  `Drop ${collection.collectionName} has exception [${e}]`,
                );
                return false;
              }),
          );
        });
      }
    });

    return Promise.all(collectionDrop)
      .then(() => Logger.log(`Drop complete`))
      .catch((e) => Logger.error(`Has [${e}] exception`));
  }

  //TODO: add only drop selected collection
}
