import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Manga, MangaDocument } from '../../modules/manga/schemas/manga.schema';
import { Command, Console } from 'nestjs-console';
import { RoleType } from '../../constant';
import {
  Author,
  AuthorDocument,
} from '../../modules/author/schemas/author.schema';
import {
  Chapter,
  ChapterDocument,
} from '../../modules/manga/schemas/chapter.schema';
import { CreateUserDTO } from '../../modules/user/dto/create-user.dto';
import {
  Favorite,
  FavoriteDocument,
} from '../../modules/user/schemas/favorite.schema';
import {
  Comment,
  CommentDocument,
} from '../../modules/comment/schemas/comment.schema';
import { User, UserDocument } from '../../modules/user/schemas/user.schema';
import { highlight, random, sampleSize } from '../utils';
import { ModelFactories } from './model-factories';
import {
  Category,
  CategoryDocument,
} from '../../modules/manga/schemas/category.schema';
import { RecommendedManga } from '../../modules/manga/schemas/recommended-manga.schema';

interface Options {
  number?: number;
  min?: number;
  max?: number;
}

const quantitySeed = {
  user: 10,
  author: 10,
  manga: 10,
};

//TODO: add seed when using new model follow existed seeder
@Injectable()
@Console({
  command: 'seed',
  description: highlight('Seed collections'),
})
export class SeederService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    @InjectModel(Manga.name) private mangaModel: Model<MangaDocument>,
    @InjectModel(Chapter.name) private chapterModel: Model<ChapterDocument>,
    @InjectModel(Author.name) private authorModel: Model<AuthorDocument>,
    @InjectModel(Favorite.name) private favoriteModel: Model<FavoriteDocument>,
    @InjectModel(Category.name) private categoryModel: Model<CategoryDocument>,
    @InjectModel(RecommendedManga.name)
    private recommendModel: Model<RecommendedManga>,
    @InjectModel(Comment.name) private commentModel: Model<CommentDocument>,
    private modelFactories: ModelFactories,
  ) {}

  @Command({
    command: 'all',
  })
  seedAll(): Promise<any> {
    return Promise.all([
      this.seedUser({ number: quantitySeed.user }),
      this.seedAuthor({ number: quantitySeed.author }),
      this.seedCategories(),
    ])
      .then(() => {
        return this.seedManga({ number: quantitySeed.manga });
      })
      .then(() => {
        return Promise.all([
          this.seedChapter(),
          this.seedFavorite(),
          this.seedComment(),
        ]);
      })
      .then(() => {
        return this.seedReplies();
      });
  }

  @Command({
    command: 'user',
    options: [
      {
        flags: '-n, --number <number of user>',
        required: false,
      },
    ],
    description: highlight(
      'Seed User Collection, it always come with one admin and one normal user account.\n' +
        'Using flag -n, --number to add more users',
    ),
  })
  seedUser(options?: Options): Promise<void> {
    const password =
      '$2b$10$hLTrfSsptmKsQVCN1qE4dubsG6SSEr.s34F1P930DgWDdKwmNmzE6';
    const numberOfUser = options?.number || 0;
    const users: CreateUserDTO[] = [];
    const admin: CreateUserDTO = {
      username: 'admin',
      password: password, //password in bcrypt
      email: 'admin@mail.com',
      role: RoleType.ADMIN,
    };
    const normalUser: CreateUserDTO = {
      username: 'user',
      password: password, //password in bcrypt
      email: 'user@mail.com',
      role: RoleType.USER,
    };
    users.push(admin, normalUser);
    users.push(...this.modelFactories.generateUser(numberOfUser));

    return this.userModel
      .insertMany(users)
      .then(() => {
        Logger.log(`Seed users complete`);
      })
      .catch((e) => {
        Logger.error(`Has ${e} exception`);
      });
  }

  @Command({
    command: 'author',
    options: [
      {
        flags: '-n, --number <number of author>',
        required: false,
      },
    ],
    description: highlight(
      'Seed authors collection.\n' +
        'By default, it will seed only one document.\n' +
        'Change it with -n, --number flag',
    ),
  })
  seedAuthor(options?: Options): Promise<void> {
    const numberOfAuthor = options?.number || 1;
    const authors = this.modelFactories.generateAuthor(numberOfAuthor);
    return this.authorModel
      .insertMany(authors)
      .then(() => {
        Logger.log('Seed authors complete');
      })
      .catch((e) => {
        Logger.error(`Has ${e} exceptions`);
      });
  }

  @Command({
    command: 'manga',
    options: [
      {
        flags: '-n, --number <number of user>',
        required: false,
      },
    ],
    description: highlight(
      'Seed mangas collection.\n' +
        'By default, it will seed only one document.\n' +
        'Change it with -n, --number flag',
    ),
  })
  async seedManga(options?: Options): Promise<void> {
    const numberOfManga = options?.number || 1;
    const mangas = this.modelFactories.generateManga(numberOfManga);
    const categories = await this.categoryModel.find({}).exec();
    const authors = await this.authorModel.find({}).exec();

    if (authors.length) {
      mangas.forEach((manga) => {
        manga.authors = sampleSize(authors, random(0, authors.length)).map(
          (author) => author._id,
        );
      });
    }

    if (categories.length) {
      mangas.forEach((manga) => {
        manga.categories = sampleSize(
          categories,
          random(0, categories.length),
        ).map((category) => category._id);
      });
    }

    return this.mangaModel
      .insertMany(mangas)
      .then(() => {
        Logger.log('Seed mangas complete');
      })
      .catch((e) => {
        Logger.error(`Has ${e} exceptions`);
      });
  }

  @Command({
    command: 'chapter',
    description: highlight(
      'Seed chapters collection for exist mangas.\n' +
        'To change quality of chapter per manga, provide both flag --min and --max',
    ),
    options: [
      {
        flags: '--min',
        required: false,
      },
      {
        flags: '--max',
        required: false,
      },
    ],
  })
  async seedChapter(options?: Options): Promise<void> {
    if (
      options &&
      ((options.min && !options.max) || (options.max && options.min))
    ) {
      Logger.error(`Give both min and max value`);
      throw new Error('Wrong flag');
    }
    const min = options?.min || 3;
    const max = options?.max || 5;

    return this.mangaModel
      .find()
      .exec()
      .then((mangas) => mangas.map((manga) => manga.id))
      .then(async (mangaIds) => {
        for (const id of mangaIds) {
          const chapters = await this.chapterModel.insertMany(
            this.modelFactories.generateChapter(random(min, max), id),
          );
          await this.mangaModel.findByIdAndUpdate(
            id,
            {
              chapters: chapters,
            },
            { upsert: true, new: true },
          );
        }
      })
      .then(() => {
        Logger.log('Seed chapters complete');
      })
      .catch((e) => {
        Logger.error(`Has ${e} exceptions`);
      });
  }

  @Command({
    command: 'favorite',
  })
  async seedFavorite(): Promise<void> {
    const mangas = await this.mangaModel.find({}).exec();
    return this.userModel
      .find({})
      .exec()
      .then(async (users) => {
        for (const user of users) {
          await this.favoriteModel.create({
            user: user,
            mangas: sampleSize(mangas, random(0, mangas.length)),
          });
        }
      })
      .then(() => {
        Logger.log('Seed favorite complete');
      })
      .catch((e) => {
        Logger.error(`Has ${e} exception`);
      });
  }

  @Command({
    command: 'categories',
  })
  seedCategories(): Promise<void> {
    return this.categoryModel
      .insertMany(this.modelFactories.generateCategories())
      .then(() => {
        Logger.log('Seed categories complete');
      })
      .catch((e) => {
        Logger.error(`Has ${e} exception`);
      });
  }

  @Command({
    command: 'recommend',
  })
  async seedRecommend(): Promise<void> {
    const mangas = await this.mangaModel.find({}).exec();
    const recommends = this.modelFactories.generateRecommend(mangas);

    return this.recommendModel
      .insertMany(recommends)
      .then(() => {
        Logger.log('Seed recommend complete');
      })
      .catch((e) => {
        Logger.error(`Has ${e} exception`);
      });
  }

  @Command({
    command: 'comment',
  })
  async seedComment(): Promise<void> {
    //TODO: query on flag
    const numberOfComment = 5;
    const users = await this.userModel.find({}).exec();
    const mangas = await this.mangaModel
      .find({}, {}, { populate: 'chapters' })
      .exec();

    const comments: Comment[] = [];

    for (const manga of mangas) {
      if (!manga.chapters) {
        continue;
      }
      for (const chapter of manga.chapters) {
        comments.push(
          ...this.modelFactories.generateComments(
            random(0, numberOfComment),
            users,
            chapter,
          ),
        );
      }
    }

    return this.commentModel
      .insertMany(comments)
      .then(() => {
        Logger.log('Seed comment complete');
      })
      .catch((e) => {
        Logger.error(`Has ${e} exception`);
      });
  }

  @Command({
    command: 'replies',
  })
  async seedReplies(): Promise<void> {
    //TODO: query on flag
    const numberOfComment = 5;

    const comments = await this.commentModel.find({}).exec();
    const users = await this.userModel.find({}).exec();
    const userIds = users.map((user) => user._id);

    try {
      for (const comment of comments) {
        const replies = await this.commentModel.insertMany(
          this.modelFactories.generateRepliesComment(
            random(0, numberOfComment),
            userIds,
          ),
        );
        comment.replies = replies;
        comment.repliesCount = replies.length;
        await comment.save();
      }

      Logger.log('Seed replies complete');
    } catch (e) {
      Logger.error(`Has ${e} exception`);
    }
  }
}
