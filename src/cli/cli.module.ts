import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { MongooseModule } from '@nestjs/mongoose';
import { ConsoleModule } from 'nestjs-console';
import { AuthorModule } from '../modules/author/author.module';
import { CommentModule } from '../modules/comment/comment.module';
import { MangaModule } from '../modules/manga/manga.module';
import { NotificationModule } from '../modules/notification/notification.module';
import { UserModule } from '../modules/user/user.module';
import { DatabaseService } from './services/database.service';
import { ModelFactories } from './services/model-factories';
import { SeederService } from './services/seeder.service';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        useNewUrlParser: true,
        uri: configService.get<string>(
          'MONGODB_CONNSTRING',
          'mongodb://admin:pass@localhost:27117',
        ),
      }),
    }),
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    ConsoleModule,
    UserModule,
    MangaModule,
    AuthorModule,
    NotificationModule,
    CommentModule,
    EventEmitterModule.forRoot(),
  ],
  providers: [SeederService, DatabaseService, ModelFactories],
  exports: [SeederService, DatabaseService],
})
export class CliModule {}
