import * as chalk from 'chalk';

export function highlight(message: string): string {
  return chalk.green(message);
}

export function sampleSize(array: Array<any>, n: number) {
  n = n == null ? 1 : n;
  const length = array == null ? 0 : array.length;
  if (!length || n < 1) {
    return [];
  }
  n = n > length ? length : n;
  let index = -1;
  const lastIndex = length - 1;
  const result = [...array];
  while (++index < n) {
    const rand = index + Math.floor(Math.random() * (lastIndex - index + 1));
    const value = result[rand];
    result[rand] = result[index];
    result[index] = value;
  }
  return result.slice(0, n);
}

export function sample(array: Array<any>): any | undefined {
  const length = array.length;
  return length > 0 ? array[Math.floor(Math.random() * length)] : undefined;
}

export function random(lo: number, hi: number): number {
  const lower = Math.max(lo, Number.MIN_SAFE_INTEGER);
  const upper = Math.min(hi, Number.MAX_SAFE_INTEGER);
  return lower + Math.round(Math.random() * (upper - lower));
}

export function upperCaseFirstLetter(s: string): string {
  return s.replace(/\w+/g, function (w) {
    return w[0].toUpperCase() + w.slice(1).toLowerCase();
  });
}
