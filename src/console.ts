import { BootstrapConsole } from 'nestjs-console';
import { CliModule } from './cli/cli.module';

const bootstrap = new BootstrapConsole({
  module: CliModule,
  useDecorators: true,
  contextOptions: {
    logger: ['log', 'error'],
  },
});

bootstrap.init().then(async (app) => {
  try {
    await app.init();
    await bootstrap.boot();
  } catch (e) {
    console.log(e);
  } finally {
    await app.close();
  }
});
