import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_FILTER } from '@nestjs/core';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { MongooseModule } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import { ConsoleModule } from 'nestjs-console';
import {
  AcceptLanguageResolver,
  I18nJsonParser,
  I18nModule,
} from 'nestjs-i18n';
import * as path from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FALLBACK_LANG_CODE } from './constant';
import { HttpExceptionFilter } from './exception-filters/http-exception.filter';
import { AuthModule } from './modules/auth/auth.module';
import { AuthorModule } from './modules/author/author.module';
import { CommentModule } from './modules/comment/comment.module';
import { MailModule } from './modules/mail/mail.module';
import { MangaModule } from './modules/manga/manga.module';
import { MeModule } from './modules/me/me.module';
import { NotificationModule } from './modules/notification/notification.module';
import { ReportModule } from './modules/report/report.module';
import { UserModule } from './modules/user/user.module';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        useNewUrlParser: true,
        uri: configService.get<string>(
          'MONGODB_CONNSTRING',
          'mongodb://admin:pass@localhost:27117',
        ),
        connectionFactory: (connection: Connection) => {
          // eslint-disable-next-line @typescript-eslint/no-var-requires
          connection.plugin(require('@meanie/mongoose-to-json'));
          return connection;
        },
      }),
    }),
    I18nModule.forRoot({
      fallbackLanguage: FALLBACK_LANG_CODE,
      fallbacks: {
        'en-*': 'en',
      },
      parser: I18nJsonParser,
      parserOptions: {
        path: path.join(__dirname, '/i18n/'), // path to build file, not this file
      },
      resolvers: [AcceptLanguageResolver],
    }),
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    EventEmitterModule.forRoot(),
    UserModule,
    AuthorModule,
    CommentModule,
    MangaModule,
    NotificationModule,
    AuthModule,
    MeModule,
    ConsoleModule,
    ReportModule,
    MailModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
  ],
})
export class AppModule {}
