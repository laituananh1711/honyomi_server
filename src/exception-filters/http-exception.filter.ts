import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  Logger,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { I18nService } from 'nestjs-i18n';

const defaultMessage: Record<string, string> = {
  en: 'Something went wrong',
  vi: 'Đã có lỗi xảy ra',
};

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  private logger = new Logger(HttpExceptionFilter.name);
  constructor(private readonly i18n: I18nService) {}

  async catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const request = ctx.getRequest<Request>();
    const response = ctx.getResponse<Response>();
    const status = exception.getStatus();

    this.logger.error(
      `Status code ${status}: ${exception.name} - ${exception.message}: ${
        request.url
      } - ${JSON.stringify(request.body)}`,
    );

    const lang = ctx.getRequest().i18nLang;

    let message = await this.i18n
      .t(exception.message, {
        lang,
      })
      .catch(() => {
        this.logger.warn(`cannot translate message ${exception.message}`);
        message = defaultMessage[lang];
      });

    this.logger.debug(`lang: ${lang}; message: ${message}`);

    response.status(status).json({
      errors: [
        {
          code: exception.name,
          detail: message,
          meta: exception.getResponse(),
        },
      ],
    });
  }
}
