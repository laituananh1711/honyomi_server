import { HttpExceptionFilter } from './http-exception.filter';

const mockI18n: any = {};

describe('HttpExceptionFilter', () => {
  it('should be defined', () => {
    expect(new HttpExceptionFilter(mockI18n)).toBeDefined();
  });
});
