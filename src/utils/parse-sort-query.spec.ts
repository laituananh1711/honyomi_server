import { parseSortQuery, ParseSortQueryOpts } from './parse-sort-query';

describe('parseSortQuery', () => {
  interface args {
    sortQuery: string | string[];
    opts: ParseSortQueryOpts;
  }

  interface TestCase {
    name: string;
    args: args;
    want: Record<string, number>;
  }

  const testCases: TestCase[] = [
    {
      name: 'should parse valid sort query string',
      args: {
        sortQuery: '+aCol',
        opts: {
          allowedCols: ['aCol'],
        },
      },
      want: {
        aCol: 1,
      },
    },
    {
      name: 'should parse multiple valid query string (string)',
      args: {
        sortQuery: '+aCol,-bCol',
        opts: {
          allowedCols: ['aCol', 'bCol'],
        },
      },
      want: {
        aCol: 1,
        bCol: -1,
      },
    },
    {
      name: 'should parse multiple valid query string (array)',
      args: {
        sortQuery: ['+aCol', '-bCol'],
        opts: {
          allowedCols: ['aCol', 'bCol'],
        },
      },
      want: {
        aCol: 1,
        bCol: -1,
      },
    },
    {
      name: 'should not parse banned cols',
      args: {
        sortQuery: '+aCol',
        opts: {
          allowedCols: [],
        },
      },
      want: {},
    },
  ];

  for (const tt of testCases) {
    it(tt.name, () => {
      const val = parseSortQuery(tt.args.sortQuery, tt.args.opts);

      expect(val).toEqual(tt.want);
    });
  }
});
