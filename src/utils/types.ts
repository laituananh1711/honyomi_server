export type base64str = string;

export type JSONResponse<Data = any, Meta = any> = {
  data?: Data;
  meta?: Meta;
  errors?: any[];
};

export type PromiseJSONResponse<Data = any, Meta = any> = Promise<
  JSONResponse<Data, Meta>
>;
