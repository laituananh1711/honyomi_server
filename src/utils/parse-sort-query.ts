import { Logger } from '@nestjs/common';

const SEPARATOR = ',';

export interface ParseSortQueryOpts {
  allowedCols: string[];
}

export type SortOptions = Record<string, 1 | -1>;

export function parseSortQuery(
  sortQueryOrCols: string | string[],
  { allowedCols }: ParseSortQueryOpts,
): SortOptions {
  const sortCols: string[] =
    typeof sortQueryOrCols === 'string'
      ? sortQueryOrCols.split(SEPARATOR)
      : sortQueryOrCols;

  const orders: SortOptions = {};

  function isColValid(colName: string): boolean {
    return allowedCols.includes(colName) && colName.length > 0;
  }

  for (const col of sortCols) {
    const sortDirection = col[0];
    const colName = col.slice(1);

    if (!isColValid(colName)) {
      Logger.debug(
        `skipping ${colName}: ${colName} not in ${JSON.stringify(allowedCols)}`,
      );
      continue;
    }

    if (sortDirection === '+') orders[colName] = 1;
    else if (sortDirection === '-') orders[colName] = -1;
    else {
      Logger.debug(`sort direction not found in ${col}`);
      continue;
    }
  }

  return orders;
}
