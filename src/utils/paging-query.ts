import { Type } from 'class-transformer';
import { IsBase64, IsNumber, IsOptional, Max } from 'class-validator';
import { base64str } from './types';

export class OffsetPagingQuery {
  @IsNumber()
  @Max(100)
  @IsOptional()
  @Type(() => Number)
  perPage = 50;

  @IsNumber()
  @IsOptional()
  @Type(() => Number)
  page = 1;
}

// See https://slack.engineering/evolving-api-pagination-at-slack
export class CursorPagingQuery {
  @IsBase64()
  @IsOptional()
  cursor?: base64str;

  @IsNumber()
  @Max(100)
  @IsOptional()
  @Type(() => Number)
  limit = 50;
}
