import * as admin from 'firebase-admin';
import { config } from 'dotenv';

config();

export const app = admin.initializeApp();

export const auth = admin.auth(app);
