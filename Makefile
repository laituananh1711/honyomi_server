start-infra:
	docker-compose --env-file .env.docker up -d
stop-infra:
	docker-compose --env-file .env.docker stop
down-infra:
	docker-compose --env-file .env.docker down
