# Honyomi Server

The server part of a manga-reading system.

## Installation

1. Install dependencies

   ```sh
   yarn
   ```

2. Add Firebase private key

   1. Go to <https://console.firebase.google.com/u/1/project/honyomi-45907/settings/serviceaccounts/adminsdk> and download the private key there.

   2. Put it in `src/serviceAccount.json`

3. Start infrastructure

   ```sh
   make start-infra
   # if you don't have `make` installed, use this command instead
   docker-compose --env-file .env.docker up -d
   ```

## Run tests

```sh
yarn test
```

## Console

### Drop collections

```shell
  # In development env
  yarn console:dev drop all
  # Or on product (require build be for run)
  yarn console drop all
```

### Seed data

Seed all collections

```shell
  # In development env
  yarn console:dev seed all
  # Or on product (require build before run)
  yarn console seed all
```

If you want to add or change, we need to take care of
the dependencies of the domain collections in `src/cli/services/seeder.service.ts`

Or seed specified collection, example

```shell
   yarn console:dev seed user
   # See more collections are supported
   yarn consoel:dev seed --help
```
